<?php

/**
 * Esta es la clase curso
 */
class Curso {
    //variables de la clase
    private $id;
    private $nombre;
    private $descripcion;
    private $foto;
    private $precio;
    private $duracion;

    //Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
    public function getId(){ return $this->id; }
    public function setId($id){ $this->id = $id; }

    //Getter y Setter del String Nombre
    public function getNombre(){ return $this->nombre; }
    public function setNombre($nombre){ $this->nombre = $nombre; }

    //Getter y Setter del String Descripcion
    public function getDescripcion(){ return $this->descripcion; }
    public function setDescripcion($descripcion){ $this->descripcion = $descripcion; }

    //Getter y Setter del String Foto
    public function getFoto(){  return $this->foto; }
    public function setFoto($foto){ $this->foto = $foto; }

    //Getter y Setter del Double Precio
    public function getPrecio(){ return $this->precio; }
    public function setPrecio($precio){ $this->precio = $precio; }

    //Getter y Setter del Integer Duracion
    public function getDuracion(){ return $this->duracion; }
    public function setDuracion($duracion){ $this->duracion = $duracion; }



    /**
    * Listara los cursos de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los cursos
    */
    public function listarCursos() {
        $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso;";
       
        $txt = $this->seleccionarCursos($sql);

        return $txt;
    }

     /**
    * Listara los cursos de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $curso es el curso a buscar en la base de datos
    * @return String $txt devolvera la vista de los curso
    */
    public function buscarCursos($curso) {
       $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso WHERE nombre LIKE '%".$curso."%';";

       $txt = $this->seleccionarCursos($sql);

       return $txt;
    }

    /**
    * Listara los cursos de la base de datos con el sql que recibe.
    * @param String $sql es el sql de la peticion
    * @return Array $listaCurso devolvera un array con los cursos
    */
    public function consultarCursos($sql) {
        $listaCurso = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $nombre, $descripcion, $foto, $precio, $duracion) = mysqli_fetch_array($res)) {
            $curso = new Curso();
            $curso->llenarCurso($id, $nombre, $descripcion, $foto, $precio, $duracion);
            array_push($listaCurso, $curso);
        }

        return $listaCurso;
    }

    /**
    * Listara los cursos de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los cursos
    */
    public function seleccionarCursos($sql) {
        $listaCurso = array();
        $listaCurso = $this->consultarCursos($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaCurso);$i++){
            $txt .= $this->tarjetaCurso($listaCurso[$i]);
        }

        return $txt;
    }

    /**
    * Listara los cursos de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $email email del usuario
    * @return String $txt devolvera la vista de los cursos
    */
    public function listaMisCursos($email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso WHERE id IN (SELECT DISTINCT Curso.id FROM Matricula, Curso WHERE Matricula.id_curso = Curso.id AND Matricula.id_usuario = ".$id.");";

        $listaCurso = array();
        $listaCurso = $this->consultarCursos($sql);

        $txt = "<h2><a href='javascript:cursoPlegable(1)'><span id='iconosPlegables1'><i class='fas fa-chevron-circle-down'></i></span> Tus Cursos</a></h2>";
        $txt .= "<span id='cursosPlegables1'>";

        if (sizeof($listaCurso) != 0) {
            for($i=0;$i<sizeof($listaCurso);$i++){
                $txt .= $this->tarjetaMiCurso($listaCurso[$i]);
            }
        }else $txt .= "<h3>Actualmente no dispones de ningun curso comprado</h3>";
        
        $txt .= "</span>";

        return $txt;
    }

    /**
    * Listara los cursos de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $email email del usuario
    * @return String $txt devolvera la vista de los cursos
    */
    public function listaOtrosCursos($email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso WHERE id NOT IN (SELECT DISTINCT Curso.id FROM Matricula, Curso WHERE Matricula.id_curso = Curso.id AND Matricula.id_usuario = ".$id.");";

        $listaCurso = array();
        $listaCurso = $this->consultarCursos($sql);

        $txt = "<h2><a href='javascript:cursoPlegable(2)'><span id='iconosPlegables2'><i class='fas fa-chevron-circle-down'></i></span> Cursos Disponibles</a></h2>";
        $txt .= "<span id='cursosPlegables2'>";

        if (sizeof($listaCurso) != 0) {
            for($i=0;$i<sizeof($listaCurso);$i++){
                $txt .= $this->tarjetaOtroCurso($listaCurso[$i]);
            }
        }else $txt .= "<h3>Actualmente no hay disponibles más cursos para comprar</h3>";

        $txt .= "</span>";

        return $txt;
    }

    /**
    * Listara los opctions con los cursos de la base de datos.
    * @param Integer $curso es la id del curso que tiene la asignatura
    * @return String $txt devolvera la vista de los cursos en options
    */
    public function selectorCursos($curso) {
        $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso;";

        $listaCurso = array();
        $listaCurso = $this->consultarCursos($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaCurso);$i++){
            $txt .= "<option value='".$listaCurso[$i]->getId()."' ".strval(($listaCurso[$i]->getId() == $curso)?'selected':'').">".$listaCurso[$i]->getNombre()."</option>";
        }

        return $txt;
    }

    /**
    * Rellenara los campos de un curso con los datos que recibe del mismo.
    * @param Integer $id
    * @param String $nombre 
    * @param String $descripcion
    * @param String $foto
    * @param Double $precio
    * @param Integer $duracion
    */
    public function llenarCurso($id, $nombre, $descripcion, $foto, $precio, $duracion){
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->foto = $foto;
        $this->precio = $precio;
        $this->duracion = $duracion;
    }

    /**
    * Devolvera la vista del curso de la base de datos.
    * @param String $curso recibe un curso
    * @return String $txt devolvera la vista del curso
    */
    public function tarjetaCurso($curso) {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='imagenContainer'><span style='background-image: url(../".$curso->getFoto().");''></span></div>";

        $txt .= "<div class='datosContainer'><p><b>".$curso->getNombre()."</b></p><p>".$curso->getDescripcion()."</p>";

        $txt .= "<p><b>Precio</b> - ".$curso->getPrecio()." €</p><p><b>Duración</b> - ".$curso->getDuracion()." Horas</p>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button onclick=window.location='cursosForm.php?id=".$curso->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraCurso(".$curso->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Devolvera la vista del curso de la base de datos.
    * @param String $curso recibe un curso
    * @return String $txt devolvera la vista del curso
    */
    public function tarjetaMiCurso($curso) {
        $txt = "<div class='tarjetaMiCurso'>
                    <div class='imagenTarjetaCurso'>
                        <span style='background-image: url(".$curso->getFoto().");'></span>
                    </div>
                    <div class='datosTarjetaCurso'>
                        <h3>".$curso->getNombre()."</h3>
                        <p>".$curso->getDescripcion()."</p>
                    </div>
                    <div class='botonTarjetaCurso'>
                        <button onclick=window.location='campus/index.php?curso=".$curso->getId()."';>Entrar al Curso</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Devolvera la vista del curso de la base de datos.
    * @param String $curso recibe un curso
    * @return String $txt devolvera la vista del curso
    */
    public function tarjetaOtroCurso($curso) {
        $txt = "<div class='tarjetaOtroCurso'>
                    <div class='imagenTarjetaCurso'>
                        <span style='background-image: url(".$curso->getFoto().");'></span>
                    </div>
                    <div class='datosTarjetaCurso'>
                        <h3>".$curso->getNombre()."</h3>
                        <p>".$curso->getDescripcion()."</p>
                    </div>
                    <div class='infoTarjetaCurso'>
                        <h3>".$curso->getPrecio()." €</h3>
                        <h3>".$curso->getDuracion()." Horas</h3>
                    </div>
                    <div class='botonTarjetaCurso'>
                        <button onclick=window.location='matriculacion.php?id=".$curso->getId()."';>Comprar Curso</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el curso que reciba por parametros
    * @param Integer $id_curso la id del curso a borrar
    */
    public function eliminarCurso($id_curso) {
        $sql = "DELETE FROM Curso WHERE id='".$id_curso."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de un curso.
    * @param $id
    */
    public function obtenerCursoPorId($id){
        $sql = "SELECT id, nombre, descripcion, foto, precio, duracion FROM Curso WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->nombre = $res['nombre'];
        $this->descripcion = $res['descripcion'];
        $this->foto = $res['foto'];
        $this->precio = $res['precio'];
        $this->duracion = $res['duracion'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo curso en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoCurso($datos) {
        $this->setNombre($datos['nombre']);
        $this->setDescripcion($datos['descripcion']);
        $this->setFoto($datos['foto']);
        $this->setPrecio($datos['precio']);
        $this->setDuracion($datos['duracion']);


        $sql = "INSERT INTO Curso(nombre, descripcion, foto, precio, duracion) VALUES ('".$this->nombre."', '".$this->descripcion."', '".$this->foto."', '".$this->precio."', '".$this->duracion."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita un curso existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarCurso($datos) {
        $this->setId($datos['id']);
        $this->setNombre($datos['nombre']);
        $this->setDescripcion($datos['descripcion']);
        $this->setFoto($datos['foto']);
        $this->setPrecio($datos['precio']);
        $this->setDuracion($datos['duracion']);

        $sql = "UPDATE Curso SET nombre='".$this->nombre."', descripcion='".$this->descripcion."', foto='".$this->foto."', precio='".$this->precio."', duracion='".$this->duracion."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Devolvera true o false en base a si el nombre que recibe la funcion mediante un formulario de curso esta siendo o no usado ya en la base de datos.
    * @param String $curso nombre del curso
    * @return Boolean $ok true o false
    */
    public function registrado($curso){
        $ok = false;
        $sql = "SELECT count(id) as nombre FROM Curso WHERE nombre='".$curso."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if ($res['nombre'] != 0) {
            $ok = true;
        }
        return $ok;
    }
}