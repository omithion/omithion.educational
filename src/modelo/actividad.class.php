<?php

/**
 * Esta es la clase actividad
 */
class Actividad {
	//variables de la clase
	private $id;
	private $id_asignatura;
	private $nombre;
	private $contenido;
	private $fecha_inicio;
	private $fecha_fin;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

	//Getter y Setter del Integer Id_asignatura
	public function getId_asignatura(){	return $this->id_asignatura; }
	public function setId_asignatura($id_asignatura){ $this->id_asignatura = $id_asignatura; }

    //Getter y Setter del String Nombre
	public function getNombre(){ return $this->nombre; }
	public function setNombre($nombre){ $this->nombre = $nombre; }

    //Getter y Setter del String Contenido
	public function getContenido(){ return $this->contenido; }
	public function setContenido($contenido){ $this->contenido = $contenido; }

    //Getter y Setter del Date Fecha_inicio
	public function getFecha_inicio(){ return $this->fecha_inicio; }
	public function setFecha_inicio($fecha_inicio){ $this->fecha_inicio = $fecha_inicio; }

    //Getter y Setter del Date Fecha_fin
	public function getFecha_fin(){ return $this->fecha_fin; }
	public function setFecha_fin($fecha_fin){ $this->fecha_fin = $fecha_fin; }

	/**
    * Listara los actividades de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los actividades
    */
    public function listarActividades() {
        $sql = "SELECT Actividad.id, Asignatura.nombre as asignatura, Actividad.nombre, Actividad.contenido, Actividad.fecha_inicio, Actividad.fecha_fin FROM Actividad, Asignatura WHERE Actividad.id_asignatura = Asignatura.id AND Asignatura.id = ";
       
        $txt = $this->seleccionarActividades($sql);

        return $txt;
    }

     /**
    * Listara los actividades de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $actividad es el actividad a buscar en la base de datos
    * @return String $txt devolvera la vista de los actividad
    */
    public function buscarActividades($actividad) {
        $sql = "SELECT Actividad.id, Asignatura.nombre as asignatura, Actividad.nombre, Actividad.contenido, Actividad.fecha_inicio, Actividad.fecha_fin FROM Actividad, Asignatura WHERE Actividad.nombre LIKE '%".$actividad."%' AND Actividad.id_asignatura = Asignatura.id AND Asignatura.id = ";
        
        $txt = $this->seleccionarActividades($sql);

        return $txt;
    }

    /**
    * Listara todos las asignaturas de la base de datos.
    * @return Array $listaAsignatura devolvera una lista con las id de las asignaturas
    */
    public function seleccionaAsignaturas() {
        $listaAsignatura = array();
    	$sql = "SELECT id FROM Asignatura;";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id) = mysqli_fetch_array($res)) {
            array_push($listaAsignatura, $id);
        }

        return $listaAsignatura;
    }

    /**
    * Listara los actividades de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los actividades
    */
    public function seleccionarActividades($sql) {
        $listaAsignatura = array();
        $txt = "";

        $listaAsignatura = $this->seleccionaAsignaturas();

    	for($i=0;$i<sizeof($listaAsignatura);$i++){
            $listaActividad = array();
	
        	$conexion = new Bd();
        	$res = $conexion->consulta($sql.$listaAsignatura[$i].";");
	
        	while (list($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin) = mysqli_fetch_array($res)) {
        	    $actividad = new Actividad();
        	    $actividad->llenarActividad($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin);
        	    array_push($listaActividad, $actividad);
        	}

            if (sizeof($listaActividad) > 0) {
                $txt .= "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(".$listaAsignatura[$i].")'><span id='iconoPlegable".$listaAsignatura[$i]."'><i class='far fa-caret-square-down'></i></span> ".$listaActividad[0]->id_asignatura."</a></p>";
                $txt .= "<span id='cursoPlegable".$listaAsignatura[$i]."'>";

                for($z=0;$z<sizeof($listaActividad);$z++){
                    $txt .= $this->tarjetaActividad($listaActividad[$z]);
                }
    
                $txt .= "</span>";
            }
		}

        return $txt;
    }

    /**
    * Rellenara los campos de un actividad con los datos que recibe de la misma.
    * @param Integer $id
    * @param String $asignatura 
    * @param String $nombre
    * @param String $contenido
    * @param Date $fecha_inicio
    * @param Date $fecha_fin
    */
    public function llenarActividad($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin){
        $this->id = $id;
        $this->id_asignatura = $asignatura;
        $this->nombre = $nombre;
        $this->contenido = $contenido;
        $this->fecha_inicio = $fecha_inicio;
        $this->fecha_fin = $fecha_fin;
    }

    /**
    * Devolvera la vista del actividad de la base de datos.
    * @param String $actividad recibe un actividad
    * @return String $txt devolvera la vista del actividad
    */
    public function tarjetaActividad($actividad, $url="") {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='datosContainer'><h2>".$actividad->getNombre()."</h2>";

        $txt .= "<p>".$actividad->getContenido()."</p>";

        $txt .= "<p><i class='far fa-calendar-alt'></i> Del <b>".$this->converirFecha($actividad->getFecha_inicio())."</b> hasta el <b>".$this->converirFecha($actividad->getFecha_fin())."</b></p>";

        $txt .= "<ul class='archivosContainer'>";
        $txt .= $this->listarArchivos($actividad->getId());
        $txt .= "</ul>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button id='moreBtn' style='background-color: #9FD120;' onclick=window.location='actividadesMore.php?".$url."id=".$actividad->getId()."';>Ver Más</button>
                        <button onclick=window.location='actividadesForm.php?".$url."id=".$actividad->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraActividad(".$actividad->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el actividad que reciba por parametros
    * @param Integer $id_actividad la id del actividad a borrar
    */
    public function eliminarActividad($id_actividad) {
        $sql = "DELETE FROM Actividad WHERE id='".$id_actividad."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de un actividad.
    * @param $id
    */
    public function obtenerActividadPorId($id){
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha_inicio, fecha_fin FROM Actividad WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_asignatura = $res['id_asignatura'];
        $this->nombre = $res['nombre'];
        $this->contenido = $res['contenido'];
        $this->fecha_inicio = $res['fecha_inicio'];
        $this->fecha_fin = $res['fecha_fin'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo actividad en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevaActividad($datos) {
        $this->setId_asignatura($datos['asignatura']);
        $this->setNombre($datos['nombre']);
        $this->setContenido($datos['contenido']);
        $this->setFecha_inicio(date('Y-m-d', time()));
        $this->setFecha_fin($datos['fecha']);

        $sql = "INSERT INTO Actividad(id_asignatura, nombre, contenido, fecha_inicio, fecha_fin) VALUES ('".$this->id_asignatura."', '".$this->nombre."', '".$this->contenido."', '".$this->fecha_inicio."', '".$this->fecha_fin."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3']);
        }
    }

    /**
    * Recoge los datos del formulario y edita un actividad existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarActividad($datos) {
        $this->setId($datos['id']);
        $this->setId_asignatura($datos['asignatura']);
        $this->setNombre($datos['nombre']);
        $this->setContenido($datos['contenido']);
        $this->setFecha_fin($datos['fecha']);

        $sql = "UPDATE Actividad SET id_asignatura='".$this->id_asignatura."', nombre='".$this->nombre."', contenido='".$this->contenido."', fecha_fin='".$this->fecha_fin."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        $sql = "DELETE FROM Archivo_Actividad WHERE id_actividad='".$this->id."';";

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1'], $this->id);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2'], $this->id);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3'], $this->id);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de una nueva actividad
    * @param String $archivo ruta del archivo
    * @param Integer $id la id de la actividad
    */
    public function subirArchivos($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Actividad;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Actividad(id_actividad, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Pintara los archivos que tenga una actividad con su archivo de extension propio
    * @param Integer $id la id de la actividad
    * @return String $id la vista de los archivos
    */
    public function listarArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Actividad WHERE id_actividad = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 28)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 28)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 28)."</p></li>";
        }

        return $txt;
    }

    /**
    * Pintara todos los archivos que tenga una actividad con su archivo de extension propio
    * @param Integer $id la id de la actividad
    * @return String $id la vista de los archivos
    */
    public function obtenerArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Actividad WHERE id_actividad = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) > 0) {
            if (sizeof($listaArchivo) == 1) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2);
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 2) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 3) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3, $listaArchivo[2], pathinfo($listaArchivo[2], PATHINFO_EXTENSION), "1E8449");
            }
        }else{
            $txt .= $this->pintarArchivo(1);
            $txt .= $this->pintarArchivo(2);
            $txt .= $this->pintarArchivo(3);
        }

        return $txt;
    }

    /**
    * Pintara los archivos que tenga una actividad con su archivo de extension propio
    * @param Integer $pos la posicion del archivo en el formulario
    * @param String $archivo la ruta del archivo
    * @param String $icon la extension del tipo de archivo
    * @param String $color el color que recibe sera el de cuando esta marcado el archivo
    * @return String $id la vista de los archivos
    */
    public function pintarArchivo($pos, $archivo='', $icon= "nofile", $color="C0392B") {
        $txt = "<ul>";

        if ($archivo != "") {
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><a href='../".$archivo."' download><span style='background-image: url(../images/archivos/".$icon.".png);''></span></a></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em; word-break: break-all;'>".substr($archivo, 28)."</li>";
        }else{
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><span style='background-image: url(../images/archivos/".$icon.".png);'></span></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em;'>Archivo no seleccionado</li>";
        }

        $txt .= "<li><input id='botonSelected".$pos."' type='button' style='background-color: #".$color.";' onclick=\"document.getElementById('fileSelected".$pos."').click()\" value='Subir Archivo'></li>
                 <li><input id='archivo".$pos."' name='archivo".$pos."' type='hidden' style='display: none;' value=''></li>
                 <li><input id='fileSelected".$pos."' type='file' style='display: none;' onchange=\"subirArchivoForm(".$pos.", 'cambiarArchivoActividad')\"></li>";
                
        $txt .= "</ul>";

        return $txt;
    }

    /**
    * Devolvera una lista de actividades que tenga una asignatura
    * @param Integer $asignatura la id de la asignatura
    * @param Integer $curso la id del curso
    * @return String $txt la lista de asignauras
    */
    public function actividadListCampus($asignatura, $curso) {
        $sql = "SELECT id, nombre, contenido, fecha_inicio, fecha_fin FROM Actividad WHERE id_asignatura = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "<p style='text-align: center;'>Aqui encontrarás las actividades disponibles en esta asignatura</p>";
        $i = 0;
        while (list($id, $nombre, $contenido, $fecha_inicio, $fecha_fin) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                        <div class='imagenTarjetaCampus'>
                            <span style='background-image: url(images/actividad.png)'></span>
                        </div>
                        <div class='datosTarjetaCampus'>
                            <h3><i class='fas fa-bookmark'></i> ".$nombre."</h3>
                            <h5>Abierta desde el ".$this->converirFecha($fecha_inicio)."</h5>
                            <h5>Hasta el ".$this->converirFecha($fecha_fin)."</h5>
                            <p>".substr($contenido, 0, 100)."...</p>
                        </div>
                        <div class='botonTarjetaCampus'>
                            <button onclick=window.location='actividad.php?curso=".$curso."&asignatura=".$asignatura."&actividad=".$id."';>Ver Más</button>
                        </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ninguna actividad disponible en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Devolvera un string con la fecha bien formateada
    * @param Date $fecha una fecha
    * @return String $txt la fecha formateada
    */
    public function converirFecha($fecha) {
        $fechaComoEntero = strtotime($fecha);
        $meses = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

        $txt = date("d", $fechaComoEntero)." de ";
        $txt .= $meses[date("m", $fechaComoEntero)-1];
        $txt .= " de ".date("Y", $fechaComoEntero);

        return $txt;
    }

    /**
    * Devolvera true o false en base a si el temario esta en esa asignatura.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function verificada($id_asignatura, $id_actividad){
        $ok = false;

        $sql = "SELECT count(id) as verificada FROM Actividad WHERE id_asignatura='".$id_asignatura."' AND id='".$id_actividad."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        if($res['verificada'] != 0){
            $ok = true;
        }
        
        return $ok;
    }

    /**
    * Pintara todos los archivos que tenga una actividad con su archivo de extension propio
    * @param String $sql el sql de los archivos
    * @return String $id la vista de los archivos
    */
    public function archivosCampus($sql) {
        $listaArchivo = array();
        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 28)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 28)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 28)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 28)."</p></li>";
        }

        return $txt;
    }

    /**
    * Recoge los datos del formulario y crea un nuevo comentario en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoComentario($datos, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);
        $sql = "INSERT INTO Respuesta_Actividad(id_actividad, id_usuario, contenido, fecha) VALUES ('".$datos['actividad']."', '".$id_usuario."', '".$datos['comentario']."', '".date('Y-m-d', time())."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivosComentario($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivosComentario($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivosComentario($datos['archivo3']);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de un nuevo comentario
    * @param String $archivo ruta del archivo
    * @param Integer $id la id del comentario
    */
    public function subirArchivosComentario($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Respuesta_Actividad;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Respuesta_Actividad(id_respuesta_actividad, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Devolvera una lista de temarios que tenga una asignatura
    * @param Integer $actividad la id de la actividad
    * @param Integer $email el email del usuario
    * @return String $txt la lista de temarios
    */
    public function actividadComentariosListCampus($actividad, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Respuesta_Actividad.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Respuesta_Actividad.contenido, Respuesta_Actividad.fecha FROM Respuesta_Actividad, Usuario WHERE Respuesta_Actividad.id_usuario = Usuario.id AND Respuesta_Actividad.id_actividad = ".$actividad." AND Respuesta_Actividad.id_usuario = ".$id_usuario.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $contenido, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>
                             <h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>
                             <h5>".$this->converirFecha($fecha)."</h5>
                             <p>".substr($contenido, 0, 140)."</p>
                             <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Respuesta_Actividad WHERE id_respuesta_actividad = ".$id.";")."</ul>
                             </div>
                         </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no tienes ninguna respuesta en esta actividad</h3>";

        return $txt;
    }

    /**
    * Devolvera una lista de temarios que tenga una asignatura
    * @param Integer $actividad la id de la actividad
    * @param Integer $email el email del usuario
    * @return String $txt la lista de temarios
    */
    public function actividadComentariosListProfesor($actividad) {
        $sql = "SELECT Respuesta_Actividad.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Respuesta_Actividad.contenido, Respuesta_Actividad.fecha FROM Respuesta_Actividad, Usuario WHERE Respuesta_Actividad.id_usuario = Usuario.id AND Respuesta_Actividad.id_actividad = ".$actividad.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $contenido, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>
                             <h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>
                             <h5>".$this->converirFecha($fecha)."</h5>
                             <p>".substr($contenido, 0, 140)."</p>
                             <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Respuesta_Actividad WHERE id_respuesta_actividad = ".$id.";")."</ul>
                             </div>
                         </div>
                         <div class='botonTarjetaCampus'>
                            <button style='background-color: #C0392B; margin: 10px;' onclick=''>Borrar</button>
                            <button style='background-color: #9FD120; margin: 10px;' onclick=''>Calificar</button>
                        </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ninguna respuesta en esta actividad</h3>";

        return $txt;
    }

    /**
    * Listara los temarios de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de los temarios
    */
    public function listarActividadesProfesor($asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha_inicio, fecha_fin FROM Actividad WHERE id_asignatura = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaActividad = array();
        while (list($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin) = mysqli_fetch_array($res)) {
            $actividad = new Actividad();
            $actividad->llenarActividad($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin);
            array_push($listaActividad, $actividad);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaActividad);$i++){
            $txt .= $this->tarjetaActividad($listaActividad[$i], $url);
        }

        if (sizeof($listaActividad) < 1) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ninguna actividad en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Listara los temarios de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $actividad es la actividad a buscar en la base de datos
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de las actividades
    */
    public function buscarActividadesProfesor($actividad, $asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha_inicio, fecha_fin FROM Actividad WHERE nombre LIKE '%".$actividad."%' AND id_asignatura = ".$asignatura.";";
        
        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaActividad = array();
        while (list($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin) = mysqli_fetch_array($res)) {
            $actividad = new Actividad();
            $actividad->llenarActividad($id, $asignatura, $nombre, $contenido, $fecha_inicio, $fecha_fin);
            array_push($listaActividad, $actividad);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaActividad);$i++){
            $txt .= $this->tarjetaActividad($listaActividad[$i], $url);
        }

        return $txt;
    }
}