<?php

/**
 * Esta es la clase tema
 */
class Tema {
	//variables de la clase
	private $id;
	private $nombre;
	private $color_principal;
	private $color_secundario;
	private $precio;
	private $ruta;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del String Nombre
	public function getNombre(){ return $this->nombre; }
	public function setNombre($nombre){	$this->nombre = $nombre; }

    //Getter y Setter del String Color_principal
	public function getColor_principal(){ return $this->color_principal; }
	public function setColor_principal($color_principal){ $this->color_principal = $color_principal; }

    //Getter y Setter del String Color_secundario
	public function getColor_secundario(){ return $this->color_secundario; }
	public function setColor_secundario($color_secundario){ $this->color_secundario = $color_secundario; }

    //Getter y Setter del Integer Precio
	public function getPrecio(){ return $this->precio; }
	public function setPrecio($precio){ $this->precio = $precio; }

    //Getter y Setter del String Ruta
	public function getRuta(){ return $this->ruta; }
	public function setRuta($ruta){ $this->ruta = $ruta; }

	/**
    * Listara los temas de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los temas
    */
    public function listarTemas() {
        $sql = "SELECT id, nombre, color_principal, color_secundario, precio, ruta FROM Tema WHERE id <> 1 AND id <> 2 AND id <> 3 AND id <> 4;";
       
        $txt = $this->seleccionarTemas($sql);

        return $txt;
    }

     /**
    * Listara los temas de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $tema es el tema a buscar en la base de datos
    * @return String $txt devolvera la vista de los tema
    */
    public function buscarTemas($tema) {
       $sql = "SELECT id, nombre, color_principal, color_secundario, precio, ruta FROM Tema WHERE nombre LIKE '%".$tema."%' AND id <> 1 AND id <> 2 AND id <> 3 AND id <> 4;";

       $txt = $this->seleccionarTemas($sql);

       return $txt;
    }

    /**
    * Listara los temas de la base de datos con el sql que recibe.
    * @param String $sql es el sql de la peticion
    * @return Array $listaTema devolvera un array con los temas
    */
    public function consultarTemas($sql) {
        $listaTema = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $nombre, $color_principal, $color_secundario, $precio, $ruta) = mysqli_fetch_array($res)) {
            $tema = new Tema();
            $tema->llenarTema($id, $nombre, $color_principal, $color_secundario, $precio, $ruta);
            array_push($listaTema, $tema);
        }

        return $listaTema;
    }

    /**
    * Listara los temas de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los temas
    */
    public function seleccionarTemas($sql) {
        $listaTema = array();
        $listaTema = $this->consultarTemas($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaTema);$i++){
            $txt .= $this->tarjetaTema($listaTema[$i]);
        }

        return $txt;
    }

    /**
    * Rellenara los campos de un tema con los datos que recibe del mismo.
    * @param Integer $id
    * @param String $nombre
    * @param String $color_principal
    * @param String $color_secundario
    * @param Double $precio
    * @param String $ruta
    */
    public function llenarTema($id, $nombre, $color_principal, $color_secundario, $precio, $ruta){
        $this->id = $id;
        $this->nombre = $nombre;
        $this->color_principal = $color_principal;
        $this->color_secundario = $color_secundario;
        $this->precio = $precio;
        $this->ruta = $ruta;
    }

    /**
    * Devolvera la vista del tema de la base de datos.
    * @param String $tema recibe un tema
    * @return String $txt devolvera la vista del tema
    */
    public function tarjetaTema($tema) {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='imagenContainer'><span style='background-color: ".$tema->getColor_principal()."; box-shadow:inset 0 0 0 8px ".$tema->getColor_secundario().";'></span></div>";

        $txt .= "<div class='datosContainer'><h2>".$tema->getNombre()."</h2>";

        $txt .= "<p><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i></p>";

        $txt .= "<p><b>".$tema->getPrecio()."</b> <i class='fas fa-coins'></i> Monedas</p>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button onclick=window.location='temasForm.php?id=".$tema->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraTema(".$tema->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el tema que reciba por parametros
    * @param Integer $id_tema la id del tema a borrar
    */
    public function eliminarTema($id_tema) {
        $sql = "DELETE FROM Tema WHERE id='".$id_tema."';";
        $sql2 = "SELECT ruta FROM Tema WHERE id = ".$id_tema.";";
        $sql3 = "UPDATE Usuario SET id_tema_actual  = 1 WHERE id_tema_actual = ".$id_tema.";";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql2);
        $conexion->consulta($sql);
        $conexion->consulta($sql3);

        unlink('../../'.$res['ruta']);
    }

    /**
    * Seleciona todos los datos de un tema.
    * @param $id
    */
    public function obtenerTemaPorId($id){
        $sql = "SELECT id, nombre, color_principal, color_secundario, precio, ruta FROM Tema WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->nombre = $res['nombre'];
        $this->color_principal = $res['color_principal'];
        $this->color_secundario = $res['color_secundario'];
        $this->precio = $res['precio'];
        $this->ruta = $res['ruta'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo tema en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoTema($datos) {
        $this->setNombre($datos['nombre']);
        $this->setColor_principal($datos['color_principal']);
        $this->setColor_secundario($datos['color_secundario']);
        $this->setPrecio($datos['precio']);
        $this->setRuta($this->crearEstilo($datos['color_principal'], $datos['color_secundario']));


        $sql = "INSERT INTO Tema(nombre, color_principal, color_secundario, precio, ruta) VALUES ('".$this->nombre."', '".$this->color_principal."', '".$this->color_secundario."', '".$this->precio."', '".$this->ruta."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita un tema existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarTema($datos) {
        $this->setId($datos['id']);
        $this->setNombre($datos['nombre']);
        $this->setColor_principal($datos['color_principal']);
        $this->setColor_secundario($datos['color_secundario']);
        $this->setPrecio($datos['precio']);
        $this->setRuta($this->crearEstilo($datos['color_principal'], $datos['color_secundario']));

        unlink('../../'.$datos['ruta']);

        $sql = "UPDATE Tema SET nombre='".$this->nombre."', color_principal='".$this->color_principal."', color_secundario='".$this->color_secundario."', precio='".$this->precio."', ruta='".$this->ruta."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Devolvera true o false en base a si el nombre que recibe la funcion mediante un formulario de tema esta siendo o no usado ya en la base de datos.
    * @param String $tema nombre del tema
    * @return Boolean $ok true o false
    */
    public function registrado($tema){
        $ok = false;
        $sql = "SELECT count(id) as nombre FROM Tema WHERE nombre='".$tema."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if ($res['nombre'] != 0) {
            $ok = true;
        }
        return $ok;
    }

    public function temasDelUsuario($email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT id, color_principal FROM Tema WHERE id IN (SELECT DISTINCT Tema.id FROM Tema, Tema_Usuario WHERE Tema_Usuario.id_tema = Tema.id AND Tema_Usuario.id_usuario = ".$id_usuario.");";
        $sql2 = "SELECT id_tema_actual FROM Usuario WHERE id = ".$id_usuario.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        $res2 = $conexion->consultaSimple($sql2);

        $txt = "";
        $i = 0;
        while (list($id, $color_principal) = mysqli_fetch_array($res)) {
            if ($res2['id_tema_actual'] == $id) {
                $txt .= "<input type='button' class='check' style='background-color: ".$color_principal."; border-color: #F39C12;' onclick=\"ajustarTema(".$i.", '".$color_principal."')\">";
                $txt .= "<input type='radio' name='tema' value='".$id."' style='display: none;' checked>";
            }else{
                $txt .= "<input type='button' class='check' style='background-color: ".$color_principal.";' onclick=\"ajustarTema(".$i.", '".$color_principal."')\">";
                $txt .= "<input type='radio' name='tema' value='".$id."' style='display: none;'>";
            }
            $i++;
        }

        return $txt;
    }

    public function cambiarTemaUsuario($tema, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "UPDATE Usuario SET id_tema_actual=".$tema." WHERE id = ".$id_usuario.";";
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    public function temasDeTienda($id_usuario) {
        $sql = "SELECT id, color_principal, precio, nombre FROM Tema WHERE id NOT IN (SELECT DISTINCT Tema.id FROM Tema, Tema_Usuario WHERE Tema_Usuario.id_tema = Tema.id AND Tema_Usuario.id_usuario = ".$id_usuario.");";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $txt = "";
        $i = 0;
        while (list($id, $color_principal, $precio, $nombre) = mysqli_fetch_array($res)) {
            $txt .= "<ul>
                         <li>
                             <input type='button' class='check' style='background-color: ".$color_principal.";' onclick=\"ajustarTemaTienda(".$i.", '".$color_principal."', ".$precio.")\">
                             <input type='radio' name='tema' value='".$id."' style='display: none;'>
                         </li>
                         <li style='text-transform: uppercase;'><b>".$nombre."</b></li>
                         <li id='precioTema'><b>".$precio." <i class='fas fa-coins'></i></b></li>
                     </ul>";
            $i++;
        }

        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun tema disponible para su compra</h3>";
        else $txt .= "<span id='comprarTemaBtn' type='button' onclick='validarTema()'>Comprar</span>";

        return $txt;
    }

    public function comprarTema($tema, $email) {
        $conexion = new Bd();
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "INSERT INTO Tema_Usuario(id_usuario, id_tema) VALUES (".$id_usuario.", ".$tema.");";
        $sql2 = "SELECT monedas FROM Usuario WHERE id = ".$id_usuario.";";
        $sql3 = "SELECT precio FROM Tema WHERE id = ".$tema.";";

        $conexion->consulta($sql);
        $res = $conexion->consultaSimple($sql2);
        $res2 = $conexion->consultaSimple($sql3);

        $monedas = $res['monedas'] - $res2['precio'];
        $sql4 = "UPDATE Usuario SET monedas=".$monedas." WHERE id = ".$id_usuario.";";
        $conexion->consulta($sql4);
    }

    public function crearEstilo($color_principal, $color_secundario){
        $archivo = "css/temas/".time().".css";
        $fh = fopen("../../".$archivo, 'w') or die("Se produjo un error al crear el archivo");
  
        $texto = "header, nav, #ayudaView button, aside, #indexCampus, #asidePlegado, #asideMovilPlegado, #newComentarioCampus input[type='button'], #comprarTemaBtn{
                      background-color: ".$color_principal.";
                  }
                  nav a:hover, #menuMovil a:hover, #menuMovil2 a:hover, .activo, #misCursos h2 a, #otrosCursos h2 a, section h1, #ayudaView h3 a, .tituloDesplegable a, #consultaTarjeta h4, aside li a:hover, #tituloH2, #precioTema{
                      color: ".$color_secundario.";
                  }
                  .botonTarjetaCurso button, .tarjetaCampus button, #redesPerfil input[type='submit'], #personalPerfil input[type='submit'], #formConsulta input[type='button'], #salirCampus a, #temaAjustes input[type='submit'], #passAjustes input[     type='button']{
                      background-color: ".$color_secundario.";
                  }
                  .infoTarjetaCurso h3, #redesPlegablePerfil a, #ajustesView h3 a, .tarjetaCampus h3{
                      color: ".$color_principal.";
                  }
                  #imagenPerfil span{
                      border-color: ".$color_secundario.";
                  }
                  footer{
                      background-color: ".$color_principal."B3;
                  }";
  
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);

        return $archivo;
    }
}