<?php

/**
 * Esta es la clase icono
 */
class Icono {
	//variables de la clase
	private $id;
	private $nombre;
	private $muestra;
	private $precio;
	private $ruta;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del String Nombre
	public function getNombre(){ return $this->nombre; }
	public function setNombre($nombre){	$this->nombre = $nombre; }

    //Getter y Setter del String Muestra
	public function getMuestra(){ return $this->muestra; }
	public function setMuestra($muestra){ $this->muestra = $muestra; }

    //Getter y Setter del Integer Precio
	public function getPrecio(){ return $this->precio; }
	public function setPrecio($precio){ $this->precio = $precio; }

    //Getter y Setter del String Ruta
	public function getRuta(){ return $this->ruta; }
	public function setRuta($ruta){ $this->ruta = $ruta; }


}