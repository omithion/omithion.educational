<?php

/**
 * Esta es la clase temario
 */
class Temario {
	//variables de la clase
	private $id;
	private $id_asignatura;
	private $nombre;
	private $contenido;
	private $fecha;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del Integer Id_temario
	public function getId_asignatura(){	return $this->id_asignatura; }
	public function setId_asignatura($id_asignatura){ $this->id_asignatura = $id_asignatura; }

    //Getter y Setter del String Nombre
	public function getNombre(){ return $this->nombre; }
	public function setNombre($nombre){	$this->nombre = $nombre; }

    //Getter y Setter del String Contenido
	public function getContenido(){ return $this->contenido; }
	public function setContenido($contenido){ $this->contenido = $contenido; }

    //Getter y Setter del Date Fecha
	public function getFecha(){ return $this->fecha; }
	public function setFecha($fecha){ $this->fecha = $fecha; }

	/**
    * Listara los temarios de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los temarios
    */
    public function listarTemarios() {
        $sql = "SELECT Temario.id, Asignatura.nombre as asignatura, Temario.nombre, Temario.contenido, Temario.fecha FROM Temario, Asignatura WHERE Temario.id_asignatura = Asignatura.id AND Asignatura.id = ";
       
        $txt = $this->seleccionarTemarios($sql);

        return $txt;
    }

    /**
    * Listara los temarios de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $temario es el temario a buscar en la base de datos
    * @return String $txt devolvera la vista de los temario
    */
    public function buscarTemarios($temario) {
        $sql = "SELECT Temario.id, Asignatura.nombre as asignatura, Temario.nombre, Temario.contenido, Temario.fecha FROM Temario, Asignatura WHERE Temario.nombre LIKE '%".$temario."%' AND Temario.id_asignatura = Asignatura.id AND Asignatura.id = ";
        
        $txt = $this->seleccionarTemarios($sql);

        return $txt;
    }

    /**
    * Listara todos las asignaturas de la base de datos.
    * @return Array $listaAsignatura devolvera una lista con las id de las asignaturas
    */
    public function seleccionaAsignaturas() {
        $listaAsignatura = array();
    	$sql = "SELECT id FROM Asignatura;";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id) = mysqli_fetch_array($res)) {
            array_push($listaAsignatura, $id);
        }

        return $listaAsignatura;
    }

    /**
    * Listara los temarios de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los temarios
    */
    public function seleccionarTemarios($sql) {
        $listaAsignatura = array();
        $txt = "";

        $listaAsignatura = $this->seleccionaAsignaturas();

    	for($i=0;$i<sizeof($listaAsignatura);$i++){
            $listaTemario = array();
	
        	$conexion = new Bd();
        	$res = $conexion->consulta($sql.$listaAsignatura[$i].";");
	
        	while (list($id, $asignatura, $nombre, $contenido, $fecha) = mysqli_fetch_array($res)) {
        	    $temario = new Temario();
        	    $temario->llenarTemario($id, $asignatura, $nombre, $contenido, $fecha);
        	    array_push($listaTemario, $temario);
        	}

            if (sizeof($listaTemario) > 0) {
                $txt .= "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(".$listaAsignatura[$i].")'><span id='iconoPlegable".$listaAsignatura[$i]."'><i class='far fa-caret-square-down'></i></span> ".$listaTemario[0]->id_asignatura."</a></p>";
                $txt .= "<span id='cursoPlegable".$listaAsignatura[$i]."'>";

                for($z=0;$z<sizeof($listaTemario);$z++){
                    $txt .= $this->tarjetaTemario($listaTemario[$z]);
                }
    
                $txt .= "</span>";
            }
		}

        return $txt;
    }

    /**
    * Rellenara los campos de un temario con los datos que recibe de la misma.
    * @param Integer $id
    * @param String $asignatura 
    * @param String $nombre
    * @param String $contenido
    * @param Date $fecha
    */
    public function llenarTemario($id, $asignatura, $nombre, $contenido, $fecha){
        $this->id = $id;
        $this->id_asignatura = $asignatura;
        $this->nombre = $nombre;
        $this->contenido = $contenido;
        $this->fecha = $fecha;
    }

    /**
    * Devolvera la vista del temario de la base de datos.
    * @param String $temario recibe un temario
    * @return String $txt devolvera la vista del temario
    */
    public function tarjetaTemario($temario, $url="") {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='datosContainer'><h2>".$temario->getNombre()."</h2>";

        $txt .= "<p>".$temario->getContenido()."</p>";

        $txt .= "<p><i class='far fa-calendar-alt'></i> Creado el <b>".$this->converirFecha($temario->getFecha())."</b></p>";

        $txt .= "<ul class='archivosContainer'>";
        $txt .= $this->listarArchivos($temario->getId());
        $txt .= "</ul>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button id='moreBtn' style='background-color: #9FD120;' onclick=window.location='temariosMore.php?".$url."id=".$temario->getId()."';>Ver Más</button>
                        <button onclick=window.location='temariosForm.php?".$url."id=".$temario->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraTemario(".$temario->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el temario que reciba por parametros
    * @param Integer $id_temario la id del temario a borrar
    */
    public function eliminarTemario($id_temario) {
        $sql = "DELETE FROM Temario WHERE id='".$id_temario."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de un temario.
    * @param $id
    */
    public function obtenerTemarioPorId($id){
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha FROM Temario WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_asignatura = $res['id_asignatura'];
        $this->nombre = $res['nombre'];
        $this->contenido = $res['contenido'];
        $this->fecha = $res['fecha'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo temario en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoTemario($datos) {
        $this->setId_asignatura($datos['asignatura']);
        $this->setNombre($datos['nombre']);
        $this->setContenido($datos['contenido']);
        $this->setFecha(date('Y-m-d', time()));


        $sql = "INSERT INTO Temario(id_asignatura, nombre, contenido, fecha) VALUES ('".$this->id_asignatura."', '".$this->nombre."', '".$this->contenido."', '".$this->fecha."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3']);
        }
    }

    /**
    * Recoge los datos del formulario y edita un temario existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarTemario($datos) {
        $this->setId($datos['id']);
        $this->setId_asignatura($datos['asignatura']);
        $this->setNombre($datos['nombre']);
        $this->setContenido($datos['contenido']);

        $sql = "UPDATE Temario SET id_asignatura='".$this->id_asignatura."', nombre='".$this->nombre."', contenido='".$this->contenido."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        $sql = "DELETE FROM Archivo_Temario WHERE id_temario='".$this->id."';";

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1'], $this->id);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2'], $this->id);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3'], $this->id);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de un nuevo temario
    * @param String $archivo ruta del archivo
    * @param Integer $id la id del temario
    */
    public function subirArchivos($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Temario;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Temario(id_temario, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Pintara los archivos que tenga un temario con su archivo de extension propio
    * @param Integer $id la id del temario
    * @return String $id la vista de los archivos
    */
    public function listarArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Temario WHERE id_temario = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 25)."</p></li>";
        }

        return $txt;
    }

    /**
    * Pintara todos los archivos que tenga un temario con su archivo de extension propio
    * @param Integer $id la id del temario
    * @return String $id la vista de los archivos
    */
    public function obtenerArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Temario WHERE id_temario = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) > 0) {
            if (sizeof($listaArchivo) == 1) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2);
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 2) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 3) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3, $listaArchivo[2], pathinfo($listaArchivo[2], PATHINFO_EXTENSION), "1E8449");
            }
        }else{
            $txt .= $this->pintarArchivo(1);
            $txt .= $this->pintarArchivo(2);
            $txt .= $this->pintarArchivo(3);
        }

        return $txt;
    }

    /**
    * Pintara los archivos que tenga un temario con su archivo de extension propio
    * @param Integer $pos la posicion del archivo en el formulario
    * @param String $archivo la ruta del archivo
    * @param String $icon la extension del tipo de archivo
    * @param String $color el color que recibe sera el de cuando esta marcado el archivo
    * @return String $id la vista de los archivos
    */
    public function pintarArchivo($pos, $archivo='', $icon= "nofile", $color="C0392B") {
        $txt = "<ul>";

        if ($archivo != "") {
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><a href='../".$archivo."' download><span style='background-image: url(../images/archivos/".$icon.".png);''></span></a></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em; word-break: break-all;'>".substr($archivo, 25)."</li>";
        }else{
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><span style='background-image: url(../images/archivos/".$icon.".png);'></span></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em;'>Archivo no seleccionado</li>";
        }

        $txt .= "<li><input id='botonSelected".$pos."' type='button' style='background-color: #".$color.";' onclick=\"document.getElementById('fileSelected".$pos."').click()\" value='Subir Archivo'></li>
                 <li><input id='archivo".$pos."' name='archivo".$pos."' type='hidden' style='display: none;' value=''></li>
                 <li><input id='fileSelected".$pos."' type='file' style='display: none;' onchange=\"subirArchivoForm(".$pos.", 'cambiarArchivoTemario')\"></li>";
                
        $txt .= "</ul>";

        return $txt;
    }

    /**
    * Devolvera una lista de temarios que tenga una asignatura
    * @param Integer $asignatura la id de la asignatura
    * @param Integer $curso la id del curso
    * @return String $txt la lista de temarios
    */
    public function temarioListCampus($asignatura, $curso) {
        $sql = "SELECT id, nombre, contenido, fecha FROM Temario WHERE id_asignatura = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "<p style='text-align: center;'>Aqui encontrarás los temarios disponibles en esta asignatura</p>";
        $i = 0;
        while (list($id, $nombre, $contenido, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                        <div class='imagenTarjetaCampus'>
                            <span style='background-image: url(images/temario.png)'></span>
                        </div>
                        <div class='datosTarjetaCampus'>
                            <h3><i class='fas fa-bookmark'></i> ".$nombre."</h3>
                            <h5>".$this->converirFecha($fecha)."</h5>
                            <p>".substr($contenido, 0, 100)."...</p>
                        </div>
                        <div class='botonTarjetaCampus'>
                            <button onclick=\"window.location='temario.php?curso=".$curso."&asignatura=".$asignatura."&temario=".$id."';\">Ver Más</button>
                        </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun temario disponible en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Devolvera un string con la fecha bien formateada
    * @param Date $fecha una fecha
    * @return String $txt la fecha formateada
    */
    public function converirFecha($fecha) {
        $fechaComoEntero = strtotime($fecha);
        $meses = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

        $txt = date("d", $fechaComoEntero)." de ";
        $txt .= $meses[date("m", $fechaComoEntero)-1];
        $txt .= " de ".date("Y", $fechaComoEntero);

        return $txt;
    }

    /**
    * Devolvera true o false en base a si el temario esta en esa asignatura.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function verificado($id_asignatura, $id_temario){
        $ok = false;

        $sql = "SELECT count(id) as verificado FROM Temario WHERE id_asignatura='".$id_asignatura."' AND id='".$id_temario."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        if($res['verificado'] != 0){
            $ok = true;
        }

        return $ok;
    }

    /**
    * Pintara todos los archivos que tenga un temario con su archivo de extension propio
    * @param String $sql el sql de los archivos
    * @return String $id la vista de los archivos
    */
    public function archivosCampus($sql) {
        $listaArchivo = array();
        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 25)."</p></li>";
        }

        return $txt;
    }

    /**
    * Recoge los datos del formulario y crea un nuevo comentario en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoComentario($datos, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);
        $sql = "INSERT INTO Comentario_Temario(id_temario, id_usuario, respuesta, fecha) VALUES ('".$datos['temario']."', '".$id_usuario."', '".$datos['comentario']."', '".date('Y-m-d', time())."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivosComentario($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivosComentario($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivosComentario($datos['archivo3']);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de un nuevo comentario
    * @param String $archivo ruta del archivo
    * @param Integer $id la id del comentario
    */
    public function subirArchivosComentario($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Comentario_Temario;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Comentario_Temario(id_comentario_temario, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Devolvera una lista de temarios que tenga una asignatura
    * @param Integer $temario la id del temario
    * @param Integer $email el email del usuario
    * @return String $txt la lista de temarios
    */
    public function temarioComentariosListCampus($temario, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Comentario_Temario.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Usuario.profesor, Comentario_Temario.respuesta, Comentario_Temario.fecha FROM Comentario_Temario, Usuario WHERE Comentario_Temario.id_usuario = Usuario.id AND Comentario_Temario.id_temario = ".$temario.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $profesor, $respuesta, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>";
            if ($profesor == 0) {
                $txt .= "<h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>";
            }else{
                $txt .= "<h4><i class='fas fa-user-secret'></i> ".$nombre." ".$apellidos."</h4>";
            }
            $txt .= "<h5>".$this->converirFecha($fecha)."</h5>
                             <p>".substr($respuesta, 0, 140)."</p>
                             <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Comentario_Temario WHERE id_comentario_temario = ".$id.";")."</ul>
                             </div>
                         </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun comentario en este temario</h3>";

        return $txt;
    }

    /**
    * Devolvera una lista de temarios que tenga una asignatura
    * @param Integer $temario la id del temario
    * @param Integer $email el email del usuario
    * @return String $txt la lista de temarios
    */
    public function temarioComentariosListProfesor($temario) {
        $usuario = new Usuario();

        $sql = "SELECT Comentario_Temario.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Usuario.profesor, Comentario_Temario.respuesta, Comentario_Temario.fecha FROM Comentario_Temario, Usuario WHERE Comentario_Temario.id_usuario = Usuario.id AND Comentario_Temario.id_temario = ".$temario.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $profesor, $respuesta, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>";
            if ($profesor == 0) {
                $txt .= "<h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>";
            }else{
                $txt .= "<h4><i class='fas fa-user-secret'></i> ".$nombre." ".$apellidos."</h4>";
            }
            $txt .= "<h5>".$this->converirFecha($fecha)."</h5>
                            <p>".substr($respuesta, 0, 140)."</p>
                            <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Comentario_Temario WHERE id_comentario_temario = ".$id.";")."</ul>
                            </div>
                            <div class='botonTarjetaCampus'>
                                <button style='background-color: #C0392B; margin: 10px;' onclick=''>Borrar</button>
                            </div>
                         </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun comentario en este temario</h3>";

        return $txt;
    }

    /**
    * Listara los temarios de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de los temarios
    */
    public function listarTemariosProfesor($asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha FROM Temario WHERE id_asignatura = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaTemario = array();
        while (list($id, $asignatura, $nombre, $contenido, $fecha) = mysqli_fetch_array($res)) {
            $temario = new Temario();
            $temario->llenarTemario($id, $asignatura, $nombre, $contenido, $fecha);
            array_push($listaTemario, $temario);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaTemario);$i++){
            $txt .= $this->tarjetaTemario($listaTemario[$i], $url);
        }

        if (sizeof($listaTemario) < 1) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun temario en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Listara los temarios de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $temario es el temario a buscar en la base de datos
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de los temario
    */
    public function buscarTemariosProfesor($temario, $asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT id, id_asignatura, nombre, contenido, fecha FROM Temario WHERE nombre LIKE '%".$temario."%' AND id_asignatura = ".$asignatura.";";
        
        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaTemario = array();
        while (list($id, $asignatura, $nombre, $contenido, $fecha) = mysqli_fetch_array($res)) {
            $temario = new Temario();
            $temario->llenarTemario($id, $asignatura, $nombre, $contenido, $fecha);
            array_push($listaTemario, $temario);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaTemario);$i++){
            $txt .= $this->tarjetaTemario($listaTemario[$i], $url);
        }

        return $txt;
    }
}