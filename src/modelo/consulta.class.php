<?php

/**
 * Esta es la clase consulta
 */
class Consulta {
	//variables de la clase
	private $id;
	private $id_usuario;
	private $fecha_creacion;
	private $titulo;
	private $descripcion;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del Integer Id_usuario
	public function getId_usuario(){	return $this->id_usuario; }
	public function setId_usuario($id_usuario){ $this->id_usuario = $id_usuario; }

    //Getter y Setter del Date Fecha_creacion
	public function getFecha_creacion(){ return $this->fecha_creacion; }
	public function setFecha_creacion($fecha_creacion){ $this->fecha_creacion = $fecha_creacion; }

    //Getter y Setter del String Titulo
	public function getTitulo(){ return $this->titulo; }
	public function setTitulo($titulo){ $this->titulo = $titulo; }

    //Getter y Setter del String Descripcion
	public function getDescripcion(){ return $this->descripcion; }
	public function setDescripcion($descripcion){ $this->descripcion = $descripcion; }

    
	/**
    * Listara las consultas de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $email email del usuario
    * @return String $txt devolvera la vista de las consultas
    */
    public function listaMisConsultas($email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Consulta.id, Consulta.id_usuario, Consulta.fecha_creacion, Consulta.titulo, Consulta.descripcion FROM Consulta WHERE Consulta.id IN (SELECT DISTINCT Consulta.id FROM Consulta_Usuario, Consulta WHERE Consulta_Usuario.id_consulta = Consulta.id AND Consulta_Usuario.id_asignado = ".$id.");";

        $txt = $this->misConsultas($sql);

        return $txt;
    }

    private function misConsultas($sql){
        $listaConsulta = array();
        $listaConsulta = $this->consultarConsultas($sql);

        $txt = "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(1)'><span id='iconoPlegable1'><i class='far fa-caret-square-down'></i></span> Consultas Asignadas</a></p>";
        $txt .= "<span id='cursoPlegable1'>";

        if (sizeof($listaConsulta) != 0) {
            for($i=0;$i<sizeof($listaConsulta);$i++){
                $txt .= $this->tarjetaMiConsulta($listaConsulta[$i]);
            }
        }else $txt .= "<h3>Actualmente no dispones de ninguna consulta asignada.</h3>";
        
        $txt .= "</span>";

        return $txt;
    }

    /**
    * Listara las consultas de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $email email del usuario
    * @return String $txt devolvera la vista de las consultas
    */
    public function listaOtrasConsultas($email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Consulta.id, Consulta.id_usuario, Consulta.fecha_creacion, Consulta.titulo, Consulta.descripcion FROM Consulta WHERE  Consulta.id NOT IN (SELECT DISTINCT Consulta.id FROM Consulta_Usuario, Consulta WHERE Consulta_Usuario.id_consulta = Consulta.id AND Consulta_Usuario.id_asignado = ".$id.");";

        $txt = $this->otrasConsultas($sql);

        return $txt;
    }

    private function otrasConsultas($sql){
        $listaConsulta = array();
        $listaConsulta = $this->consultarConsultas($sql);

        $txt = "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(2)'><span id='iconoPlegable2'><i class='far fa-caret-square-down'></i></span> Consultas Sin Asignar</a></p>";
        $txt .= "<span id='cursoPlegable2'>";

        if (sizeof($listaConsulta) != 0) {
            for($i=0;$i<sizeof($listaConsulta);$i++){
                $txt .= $this->tarjetaOtraConsulta($listaConsulta[$i]);
            }
        }else $txt .= "<h3>Actualmente no hay ninguna consulta pendiente de asignar.</h3>";

        $txt .= "</span>";

        return $txt;
    }

    /**
    * Devolvera la vista de la consulta de la base de datos.
    * @param String $consulta recibe una consulta
    * @return String $txt devolvera la vista de la consulta
    */
    public function tarjetaMiConsulta($consulta) {
        $txt = "<div class='tarjetaContainer'>
					<div class='datosContainer' style='margin-left: 25px;'>
						<h2>".$consulta->getTitulo()."</h2>
						<p>".$consulta->getDescripcion()."</p>
						<h4 style='font-weight: normal;'>Escrito por <b>".$this->obtenerNombreApellidos($consulta->getId_usuario())."</b></h4>
						<h5 style='margin-top: 10px; font-weight: normal;'>Creado el <b>".$this->converirFecha($consulta->getFecha_creacion())."</b></h5>
					</div>
					<div class='botonesContainer'>
						<button onclick=\"window.location='consultasForm.php?id=".$consulta->getId()."';\">Ver</button>
						<button style='background-color: #C0392B;' onclick=\"window.location='../src/controlador/consulta.main.php?opcion=3&id_consulta=".$consulta->getId()."';\">Liberar</button>
					</div>
				</div>";

        return $txt;
    }

    /**
    * Devolvera la vista de la consulta de la base de datos.
    * @param String $consulta recibe una consulta
    * @return String $txt devolvera la vista de la consulta
    */
    public function tarjetaOtraConsulta($consulta) {
        $txt = "<div class='tarjetaContainer'>
					<div class='datosContainer' style='margin-left: 25px;'>
						<h2>".$consulta->getTitulo()."</h2>
						<p>".$consulta->getDescripcion()."</p>
						<h4 style='font-weight: normal;'>Escrito por <b>".$this->obtenerNombreApellidos($consulta->getId_usuario())."</b></h4>
						<h5 style='margin-top: 10px; font-weight: normal;'>Creado el <b>".$this->converirFecha($consulta->getFecha_creacion())."</b></h5>
					</div>
					<div class='botonesContainer'>
						<button style='background-color: #9FD120;' onclick=\"window.location='../src/controlador/consulta.main.php?opcion=2&id_consulta=".$consulta->getId()."';\">Asignar</button>
						<button style='background-color: #C0392B;' onclick='borraConsulta(".$consulta->getId().")'>Eliminar</button>
					</div>
				</div>";

        return $txt;
    }

    /**
    * Listara las consultas de la base de datos con el sql que recibe.
    * @param String $sql es el sql de la peticion
    * @return Array $listaConsulta devolvera un array con las consultas
    */
    public function consultarConsultas($sql) {
        $listaConsulta = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $id_usuario, $fecha_creacion, $titulo, $descripcion) = mysqli_fetch_array($res)) {
            $consulta = new Consulta();
            $consulta->llenarConsulta($id, $id_usuario, $fecha_creacion, $titulo, $descripcion);
            array_push($listaConsulta, $consulta);
        }

        return $listaConsulta;
    }

    /**
    * Rellenara los campos de una consulta con los datos que recibe del mismo.
    * @param Integer $id
    * @param Integer $id_usuario 
    * @param Date $fecha_creacion
    * @param String $titulo
    * @param String $descripcion
    */
    public function llenarConsulta($id, $id_usuario, $fecha_creacion, $titulo, $descripcion){
        $this->id = $id;
        $this->id_usuario = $id_usuario;
        $this->fecha_creacion = $fecha_creacion;
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
    }

    /**
    * Recoge los datos del formulario y crea una nueva consulta en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevaConsulta($datos) {
        $this->setId_usuario($datos['id']);
        $this->setFecha_creacion(date('Y-m-d', time()));
        $this->setTitulo($datos['titulo']);
        $this->setDescripcion($datos['descripcion']);

        $sql = "INSERT INTO Consulta(id_usuario, fecha_creacion, titulo, descripcion) VALUES ('".$this->id_usuario."', '".$this->fecha_creacion."', '".$this->titulo."', '".$this->descripcion."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del boton de asignar y de la variable de sesion y crea una nueva consulta_usuario en la base de datos.
    * @param Integer $consulta id de la consulta a asignar
    * @param String $email email del usuario
    */
    public function asginarConsulta($consulta, $email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "INSERT INTO Consulta_Usuario(id_asignado, id_consulta) VALUES ('".$id."', '".$consulta."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del boton de asignar y de la variable de sesion y elimina una consulta_usuario en la base de datos.
    * @param Integer $consulta id de la consulta a asignar
    * @param String $email email del usuario
    */
    public function liberarConsulta($consulta, $email) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "DELETE FROM Consulta_Usuario WHERE id_asignado='".$id."' AND id_consulta='".$consulta."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Envia por correo mediante PHPMailer el menaje de consulta que se requiera al correo del usuario.
    * @param String $email variable email con el email del usuario
    * @param String $contenido sera el contenido d ela respuesta que le de el administrador, o en su defecto el menaje predefinido
    */
    public function consultaCorreo($email, $contenido="Hemos recibido tu consulta correctamente, en cuanto el soporte la reciba tratará tu consulta rapidamente y te respondra lo antes posible"){
        $correo = new GestorMail();

        $tumensaje = "<div style='width: 76%; margin: 2% 10%; text-align: center; font-family: arial; font-weight: bold; box-shadow: 0 0 1em #797D7F; padding: 2%; border-radius: 10px; color: black;'>
            <a href='http://omithioneducational.com/'><img style='width: 40%;' src='http://omithioneducational.com/images/logo.png'></a>
            <p style='font-size: 1.25em; margin: 100px 0;'>".$contenido."</p>
            <hr style='height:1px;border:none;color:#333;background-color:#ddd;'>
            <a href='https://twitter.com/Omithion'><img style='width: 50px; margin: 5px;' src='http://omithiontech.com/images/twitter.png'></a>
            <p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
            </div>";

        $correo->enviar($email," Consulta - Omithion Educational", $tumensaje);
    }

    /**
    * Borrara la consulta que reciba por parametros
    * @param Integer $id_consulta la id de la consulta a borrar
    */
    public function eliminarConsulta($id_consulta) {
        $sql = "DELETE FROM Consulta WHERE id='".$id_consulta."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de una consulta.
    * @param $id
    */
    public function obtenerConsultaPorId($id){
        $sql = "SELECT id, id_usuario, fecha_creacion, titulo, descripcion FROM Consulta WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_usuario = $res['id_usuario'];
        $this->fecha_creacion = $res['fecha_creacion'];
        $this->titulo = $res['titulo'];
        $this->descripcion = $res['descripcion'];
    }

    /**
    * Devolvera el nombre completo de un usuario en base a su id en la base de datos.
    * @param Integer $id la id de un usuario
    * @return String $nombreCompleto devuelve el nombre y los apellidos
    */
    public function obtenerNombreApellidos($id){
        $sql = "SELECT nombre, apellidos FROM Usuario WHERE id='".$id."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $nombreCompleto = $res['nombre']." ".$res['apellidos'];

        return $nombreCompleto;
    }

    /**
    * Devolvera el nombre completo de un usuario en base a su id en la base de datos.
    * @param Integer $id la id de un usuario
    * @return String $res['email'] devuelve el nombre y los apellidos
    */
    public function obtenerEmail($id){
        $sql = "SELECT email FROM Usuario WHERE id='".$id."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        return $res['email'];
    }

    /**
    * Buscara a los administradores de la aplicacion y les enviara un correo a cada uno con los datos de la nueva consulta
    * @param Array $datos datos que recibe del formulario
    */
    public function adminCorreo($datos){
        $sql = "SELECT email FROM Usuario WHERE admin=1;";

        $listaEmail = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($email) = mysqli_fetch_array($res)) {
            array_push($listaEmail, $email);
        }

        $contenido = "Titulo de la consulta
            	<br>
            	<span style='font-weight: normal;'>".$datos['titulo']."</span>
            	<br><br>
        		Descripción de la consulta
        		<br>
        		<span style='font-weight: normal;'>".$datos['descripcion']."</span>";

        for($i=0;$i<sizeof($listaEmail);$i++){
            $txt .= $this->consultaCorreo($listaEmail[$i], $contenido);
        }
    }

    /**
    * Listara las consultas de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $email email del usuario
    * @param String $consulta consulta que busca el usuario
    * @return String $txt devolvera la vista de las consultas
    */
    public function buscarConsultas($email, $consulta) {
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Consulta.id, Consulta.id_usuario, Consulta.fecha_creacion, Consulta.titulo, Consulta.descripcion FROM Consulta WHERE titulo LIKE '%".$consulta."%' AND Consulta.id IN (SELECT DISTINCT Consulta.id FROM Consulta_Usuario, Consulta WHERE Consulta_Usuario.id_consulta = Consulta.id AND Consulta_Usuario.id_asignado = ".$id.");";

        $txt = $this->misConsultas($sql);

        $sql = "SELECT Consulta.id, Consulta.id_usuario, Consulta.fecha_creacion, Consulta.titulo, Consulta.descripcion FROM Consulta WHERE titulo LIKE '%".$consulta."%' AND Consulta.id NOT IN (SELECT DISTINCT Consulta.id FROM Consulta_Usuario, Consulta WHERE Consulta_Usuario.id_consulta = Consulta.id AND Consulta_Usuario.id_asignado = ".$id.");";

        $txt .= $this->otrasConsultas($sql);

        return $txt;
    }

    //Devolvera un string con la fecha bien formateada
    private function converirFecha($fecha) {
        $fechaComoEntero = strtotime($fecha);
        $meses = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

        $txt = date("d", $fechaComoEntero)." de ";
        $txt .= $meses[date("m", $fechaComoEntero)-1];
        $txt .= " de ".date("Y", $fechaComoEntero);

        return $txt;
    }
}