<?php

class GestorMail {

    
    private $mailDestinatario;
    private $asunto;
    private $mensaje;
    private $mail;
    private $error;



    public function __construct() {
        $this->mail = new PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        $this->mail->SMTPDebug = 0;
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPSecure = 'ssl'; 
        $this->mail->Host = "omithion.com"; 
        $this->mail->Port = 465; 
        $this->mail->IsHTML(true);
        $this->mail->CharSet  ="utf-8";
        $this->mail->Username = "no-reply@omithion.com"; 
        $this->mail->Password = "wBV9utpNtH"; 
        $this->mail->SetFrom("no-reply@omithion.com", "Omithion Educational");
    }
    
 
    
    public function getMailDestinatario() {
        return $this->mailDestinatario;
    }

    public function getAsunto() {
        return $this->asunto;
    }

    public function getMensaje() {
        return $this->mensaje;
    }

    public function setMailDestinatario($mailDestinatario) {
        $this->mailDestinatario = $mailDestinatario;


         $this->mail->AddAddress($this->mailDestinatario);
  
    }

    public function setAsunto($asunto) {
        $this->asunto = $asunto;
         $this->mail->Subject =  $this->asunto; 
    }

    public function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
        
         $this->mail->Body =  $this->mensaje;
        
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }

    /**
     * @param $mailDestinatario
     * @param $asunto
     * @param $mensaje
     * @return bool
     * metodo principal
     */
    public function enviar($mailDestinatario, $asunto,$mensaje){
        
        $this->setMailDestinatario($mailDestinatario);
        $this->setAsunto($asunto);
        $this->setMensaje($mensaje);
        
        
          if ( $this->mail ->Send()){
              $this->error = false;
              return true;
          } else{
               $this->error = $this->mail->ErrorInfo;
               echo $this->mail->ErrorInfo;
               return false;
              
           
           }
        
        
    }
    
    
    public function mensajeContacto($datos) {
        $txt = "<div>Nombre: ".$datos['nombre']."</div>";
        $txt .= "<div>Email: ".$datos['mail']."</div>";
        $txt .= "<br>";
        $txt .= "<div>Asunto: ".$datos['asunto']."</div>";
        $txt .= "<div>Consulta: ".$datos['consulta']."</div>";

        return $txt;
    }
    
}
