<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../modelo/consulta.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		nueva_consulta();
		break;
	case 2:
		asignar_consulta();
		break;
	case 3:
		liberar_consulta();
		break;
	case 4:
		eliminar_consulta();
		break;
	case 5:
		responder_consulta();
		break;
	
	default:
		# code...
		break;
}

function nueva_consulta(){
	session_start();
	$consulta = new Consulta();

	if (isset($_POST) && !empty($_POST)) {
		$consulta->nuevaConsulta($_POST);

		$consulta->consultaCorreo($_SESSION["user"]);
		$consulta->adminCorreo($_POST);
	}
	header("location:../../".$_POST['url']);
}

function asignar_consulta(){
	session_start();
	$consulta = new Consulta();

	if (isset($_GET) && !empty($_GET)) {
		$consulta->asginarConsulta($_GET["id_consulta"], $_SESSION["user"]);
	}
	header("location:../../administration/consultasList.php");
}

function liberar_consulta(){
	session_start();
	$consulta = new Consulta();

	if (isset($_GET) && !empty($_GET)) {
		$consulta->liberarConsulta($_GET["id_consulta"], $_SESSION["user"]);
	}
	header("location:../../administration/consultasList.php");
}

function eliminar_consulta(){
	$consulta = new Consulta();

	if (isset($_GET) && !empty($_GET)) {
		$consulta->eliminarConsulta($_GET["id_consulta"]);
	}
	header("location:../../administration/consultasList.php");
}

function responder_consulta(){
	$consulta = new Consulta();

	if (isset($_POST) && !empty($_POST)) {
		$consulta->consultaCorreo($_POST["email"], $_POST["respuesta"]);
		
		$consulta->eliminarConsulta($_POST["id"]);
	}
	header("location:../../administration/consultasList.php");
}

?>