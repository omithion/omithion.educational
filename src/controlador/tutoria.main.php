<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../modelo/tutoria.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		tutoria_form();
		break;
	case 2:
		eliminar_tutoria();
		break;
	case 3:
		comentario_tutoria();
		break;
	
	default:
		# code...
		break;
}

function tutoria_form() {
	$tutoria = new Tutoria();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$tutoria->nuevaTutoria($_POST);
    		header("location:../../".$_POST['url']);
		}else{
			$tutoria->editarTutoria($_POST);
			header("location:../../".$_POST['url']);
	    }
	}else{
		header("location:../../administration/tutoriasForm.php");
	}
}

function eliminar_tutoria() {
	if ($_GET["id_tutoria"] != "") {
		$tutoria = new Tutoria();
		$tutoria->eliminarTutoria($_GET["id_tutoria"]);
		header("location:../../".$_GET['url']);
	}else{
		header("location:../../".$_GET['url']);
	}
}

function comentario_tutoria() {
	session_start();
	$tutoria = new Tutoria();
	if (isset($_POST) && !empty($_POST)) {
    	$tutoria->nuevoComentario($_POST, $_SESSION['user']);
		header("location:../../campus/tutoria.php?curso=".$_POST['curso']."&asignatura=".$_POST['asignatura']."&tutoria=".$_POST['tutoria']."");
	}else{
		header("location:../../index.php");
	}
}

?>