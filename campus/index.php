<?php
	require_once '../src/modelo/usuario.class.php';
	require_once '../src/modelo/matricula.class.php';
    require_once '../src/modelo/asignatura.class.php';
	require_once '../src/conector/bd.class.php';

	session_start();
	if(isset($_GET['curso']) && !empty($_GET['curso'])){
		if (!isset($_SESSION['user'])) {
    	    header("location:../login.php");
    	}else if (!Matricula::verificada($_SESSION['user'], $_GET['curso'])) {
    		header("location:../index.php");
    	}elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
    	    header("location:../administration/index.php");
    	}elseif (Usuario::usuarioProfesor($_SESSION['user'])) {
    	    header("location:profesorado/index.php");
    	}
	}else header("location: ../index.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body id="indexCampus">
	<section style="width: 90%; padding-top: 2%;">
		<div id="salirCampus"><a href="../index.php">Salir del campus</a></div>

		<h1 style="color: #FFF; text-transform: uppercase; margin: 50px 0;">Asignaturas disponibles</h1>

		<?php
			$asignaturas = new Asignatura();
			echo $asignaturas->asignaturasIndex($_GET['curso']);
		?>

	</section>
</body>
</html>