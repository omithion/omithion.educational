<?php
require 'includes/verificacion.inc.php';
require_once '../src/modelo/tutoria.class.php';
$usuario = new Usuario();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Perfil | Omithion Educational</title>
    <?php include 'includes/head.inc.php';?>
    <?php include 'includes/tema.head.inc.php';?>
    <script type="text/javascript">
        var sectionSize;
        window.onload = function (){
            document.getElementById('icono6').className = "activo";
            cambiarMenuAside(1);
            sectionSize = document.getElementsByTagName('section')[0].style.width;
        }
    </script>
</head>
<body>
    <section>
        <h1>Zona de Tutorias</h1>
        <p style='text-align: center;'>Aqui encontrarás los tutorias disponibles en esta asignatura</p>
        <button id="nuevoButton" onclick="muestraNuevo()">Nueva Tutoria</button>

        <div id="newComentarioCampus">
            <form name="comentario_form" action="../src/controlador/tutoria.main.php?opcion=1" method="post">
                <input type="hidden" name="id" value="0">
                <input type="hidden" name="alumno" value="<?php echo $usuario->obtenerIdPorEmail($_SESSION['user']) ?>">
                <input type="hidden" name="url" value="<?php echo 'campus/tutorias.php?curso='.$_GET['curso'].'&asignatura='.$_GET['asignatura']; ?>">
                <input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura']; ?>">
                <h2 id="tituloH2">Nueva tutoria</h2>
                <ul>
                    <li><label id="nuevoLabel1">Título</label></li>
                    <li><input type="text" name="titulo" placeholder="Escribe un título"></li>
                </ul>
                <ul>
                    <li><label id="nuevoLabel2">Descripción</label></li>
                    <li>
                        <textarea name="descripcion" placeholder="Escribe aqui la descripción" maxlength="140"></textarea>
                    </li>
                </ul>
                <div id="fileForm">
                    <input id="numSelect" type="hidden">
                    <ul>
                        <li id="spanImage1" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName1" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected1" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected1').click()" value="Subir Archivo"></li>
                        <li><input id="archivo1" name="archivo1" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected1" type="file" style="display: none;" onchange="subirArchivoForm(1, 'cambiarArchivoForo')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage2" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName2" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected2" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected2').click()" value="Subir Archivo"></li>
                        <li><input id="archivo2" name="archivo2" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected2" type="file" style="display: none;" onchange="subirArchivoForm(2, 'cambiarArchivoForo')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage3" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName3" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected3" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected3').click()" value="Subir Archivo"></li>
                        <li><input id="archivo3" name="archivo3" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected3" type="file" style="display: none;" onchange="subirArchivoForm(3, 'cambiarArchivoForo')"></li>
                    </ul>
                </div>
                <input type="button" value="Enviar" onclick="nuevoForoTutoria()">
            </form>
        </div>

        <?php
            $tutorias = new Tutoria();
            echo $tutorias->tutoriaListCampus($_GET['asignatura'], $_GET['curso'], $usuario->obtenerIdPorEmail($_SESSION['user']));
        ?>

    </section>
    <?php include 'includes/aside.inc.php';?>
    <div id="alertaPantalla" onclick="cerrarAlerta()"></div>
    <div id="alertaMensaje">
        <h2 id="alertaH2"></h2>
        <h3 id="alertaH3"></h3>
        <p id="alertaP"></p>
        <button onclick="cerrarAlerta()">Cerrar</button>
    </div>
</body>
</html>