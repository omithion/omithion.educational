<?php
require 'includes/verificacion.inc.php';
require_once '../src/modelo/tema.class.php';
$usuario = new Usuario();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tienda | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono5').className = "activo";
			cambiarMenuAside(1);
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}

		function ajustarTemaTienda(pos, color, precio) {
		    for (var i = 0; i < document.getElementsByClassName('check').length; i++) {
		        document.getElementsByClassName('check')[i].style.borderColor = "#909497";
		    }
		
		    document.getElementsByName('tema')[pos].checked = true;
		    document.getElementsByTagName('aside')[0].style.backgroundColor = color;
		    document.getElementById('asidePlegado').style.backgroundColor = color;
		    document.getElementById('asideMovilPlegado').style.backgroundColor = color;
			document.getElementById('temaAdquirido').value = precio;
		    
		    document.getElementsByClassName('check')[pos].style.borderColor = "#F39C12";
		}

		function validarTema() {
			if(document.getElementById('temaAdquirido').value != 0){
				if (document.getElementById('monedasActual').value > document.getElementById('temaAdquirido').value-1) {
					document.tiendaDeTemas.submit();
				}else alertaPantalla("Monedas insuficientes", "No dispones de las monedas necesarias para comprar este tema.", "Intentalo de nuevo cuando hayas conseguido más monedas realizando actividades.");
			}else alertaPantalla("Seleccionar un tema", "Debes seleccionar un tema para continuar.", "Intentalo de nuevo selecionando un tema de los disponibles.");
		}
	</script>
	<style type="text/css">
		#tiendaTemas{
			width: 75%;
			margin: 25px 10%;
			padding: 15px 2.5%;
			background-color: #ECF0F1;
			border-radius: 20px;
    		box-shadow: 0 0 1em #979A9A;
    		overflow: hidden;
		}
		#comprarTemaBtn{
			clear: both;
			width: 30%;
			margin: 30px 35%;
			padding: 8px 0;
			font-weight: bold;
			font-size: 1em;
			border: none;
			border-radius: 15px;
			float: right;
			color: #FFF;
			text-shadow: 1.1px 1.1px 1.1px #7A7A7A;
			transition: all 800ms ease;
		}
		#comprarTemaBtn:hover{
			-webkit-transform:scale(1.05);
    		transform:scale(1.05);
    		box-shadow: 0 0 1em #797D7F;
		}
		@media screen and (max-width:1024px){
			#tiendaTemas{
				width: 95%;
				margin: 80px 0;
				padding: 30px 2.5%;
				font-size: 2em;
				border-radius: 30px;
			}
			#tituloH2{
				font-size: 1.5em;
			}
			#comprarTemaBtn{
				width: 60%;
				margin: 30px 20%;
				padding: 15px 0;
				font-size: 1.5em;
				border-radius: 30px;
			}
		}
		@media screen and (max-width:700px){
			#tiendaTemas{
				margin: 50px 0;
				padding: 15px 2.5%;
				font-size: 0.9em;
				border-radius: 20px;
			}
			#comprarTemaBtn{
				margin: 15px 20%;
				padding: 8px 0;
				font-size: 1.3em;
				border-radius: 15px;
			}
		}
	</style>
</head>
<body>
	<section>
		<h1>Tienda del Campus</h1>
		<p style="text-align: center;">Si buscas cambiar el estilo de la aplicación, no dudes en visita la tienda.</p>
		<div id="tiendaTemas">
			<h2 id="tituloH2">Temas Disponibles</h2>
			<div id="completaTema">
				<form name="tiendaDeTemas" action="../src/controlador/tema.main.php?opcion=4" method="post">
					<input type="hidden" name="url" value="<?php echo 'campus/tienda.php?curso='.$_GET['curso'].'&asignatura='.$_GET['asignatura'] ?>">
					<?php
						$temas = new Tema();
						echo $temas->temasDeTienda($usuario->obtenerIdPorEmail($_SESSION['user']));
					?>
				</form>
				<input type="hidden" id="temaAdquirido" value="0">
				<?php
					$monedas = new Usuario();
					echo "<input type='hidden' id='monedasActual' value='".$monedas->obtenerMonedas($_SESSION['user'])."'>";
				?>
			</div>
		</div>
	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>