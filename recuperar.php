<?php
require 'src/modelo/usuario.class.php';

$usuario = new Usuario();

if(isset($_GET['code']) && !empty($_GET['code'])){
    if(isset($_GET['id']) && !empty($_GET['id'])){

	}else{
		header("location:login.php");
	}
}else{
	header("location:login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Recuperar Contraseña | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
</head>
<body>
	<header id="headerLogin">
		<div id="atrasLogin"><a href="login.php"><i class="fas fa-arrow-left"></i></a></div>
		<img id="imagenLogin" class="imagenReducidaLogin" src="images/logo_blanco.png">
	</header>
	<section id="sectionLogin">
		<div id="loginLogin" style="display: inline-block;">
			<h1 style="color: #FFBA00;">Nueva Contraseña</h1>
			<form name="recuperar" action="" method="post">
				<input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
				<input type="hidden" name="code" value="<?php echo $_GET['code'] ?>">
				<ul>
					<li><label class="labelLogin"><span class="alertaLogin"><i class="fas fa-exclamation-circle"></i></span> Nueva contraseña</label></li>
					<li><input type="password" name="pass" onblur="login_vacio(this, 0)" placeholder="Escribe una contraseña"></li>
				</ul>
				<ul>
					<li><label class="labelLogin"><span class="alertaLogin"><i class="fas fa-exclamation-circle"></i></span> Repetir contraseña</label></li>
					<li><input type="password" name="pass2" onblur="login_vacio(this, 1)" placeholder="Repite la contraseña"></li>
				</ul>
				<input type="button" name="enviar" onclick="validar_recuperar()" value="Cambiar" style="background-color: #FFBA00;">
			</form>
		</div>
	</section>
	<div id="alertaPantalla" onclick="window.location='login.php';"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="window.location='login.php';">Ir al Login</button>
	</div>
	<footer id="footerLogin">
		<p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
	</footer>
</body>
</html>