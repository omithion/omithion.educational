<?php
	require_once 'src/modelo/usuario.class.php';
	require_once 'src/conector/bd.class.php';

	session_start();
    if (!isset($_SESSION["user"])) {
        header("location:login.php");
    }else if (Usuario::verificado($_SESSION["user"])) {
    	header("location:index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var pos = true;

		function cambia(boton) {
			if (pos) {
				document.getElementById('verificacionEmail').style.display = "block";
				document.getElementById('verificacionCodigo').style.display = "none";
				boton.style.backgroundColor = "#9FD120";
				boton.innerHTML = "Introducir código";
			}else{
				document.getElementById('verificacionEmail').style.display = "none";
				document.getElementById('verificacionCodigo').style.display = "block";
				boton.style.backgroundColor = "#FFBA00";
				boton.innerHTML = "Cambiar email";
			}
			pos = !pos;	
		}
	</script>
</head>
<body>
	<header>
		<img src="images/logo_blanco.png">
		<div id="menuMovil">
			<a href="javascript:menuMovil(1)"><i class="fas fa-bars fa-lg"></i></a>
		</div>
		<div id="menuMovil2">
			<a href="javascript:menuMovil(2)"><i class="fas fa-times fa-lg"></i></a>
		</div>
		<nav>
			<a href="src/controlador/usuario.main.php?opcion=2">
				<ul>
					<li><i class="fas fa-power-off fa-lg"></i></li>
					<li>Salir</li>
				</ul>
			</a>
		</nav>
	</header>
	<section>
		<h1 style="color: #FFBA00;">Verifica tu email</h1>
		<p style="text-align: center;">Hemos enviado un codigo de verificacion al correo: <b><?php echo $_SESSION['user'];?></b> Por favor, comprueba tu bandeja de entrada y carpetas de correo no deseado.</p>
		<div id="verificacionCodigo">
			<form name="verificacion" action="" method="get">
				<input id="codigo" type="text" name="codigo" placeholder=" - - - - -" maxlength="5" required="required">
				<input type="button" name="enviar" onclick="compruebaCodigo()" value="Enviar">
			</form>
		</div>
		<div id="verificacionEmail">
			<form name="cambio_email" action="" method="get">
				<input type="text" id="email" name="email" placeholder="Nuevo email">
				<input type="button" name="enviar" onclick="cambiarEmail()" value="Cambiar">
			</form>
		</div>
		<div id="verificacionBotonera">
			<button onclick="cambia(this)">Cambiar email</button>
			<form style="display: inline;" name="volverEnviar" action="src/controlador/usuario.main.php?opcion=5" method="post">
				<button onclick="document.volverEnviar.submit()">Volver a enviar código</button>
			</form>
		</div>
	</section>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>