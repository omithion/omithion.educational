<header>
		<img src="../images/logo_blanco.png">
		<div id="menuMovil">
			<a href="javascript:menuMovil(1)"><i class="fas fa-bars fa-lg"></i></a>
		</div>
		<div id="menuMovil2">
			<a href="javascript:menuMovil(2)"><i class="fas fa-times fa-lg"></i></a>
		</div>
		<nav>
			<a id="opcion1" href="index.php">
				<ul>
					<li><i class="fab fa-fort-awesome-alt fa-lg"></i></li>
					<li>Inicio</li>
				</ul>
			</a>
			<a id="opcion2" href="ajustes.php">
				<ul>
					<li><i class="fas fa-cog fa-lg"></i></li>
					<li>Ajustes</li>
				</ul>
			</a>
			<a id="opcion3" href="ayuda.php">
				<ul>
					<li><i class="fas fa-question-circle fa-lg"></i></li>
					<li>Ayuda</li>
				</ul>
			</a>
			<a href="../src/controlador/usuario.main.php?opcion=2">
				<ul>
					<li><i class="fas fa-power-off fa-lg"></i></li>
					<li>Salir</li>
				</ul>
			</a>
		</nav>
	</header>