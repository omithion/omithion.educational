<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/consulta.class.php';

$consulta = new Consulta();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Consultas | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			tituloPlegable(2);
		}
	</script>
	<style type="text/css">
		.datosContainer {
    			width: 80%;
			}
		@media screen and (max-width: 1920px){
			.datosContainer {
    			width: 80%;
			}
		}
		@media screen and (max-width:1024px){
			.datosContainer{
				width: auto;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarConsulta')">
			</form>
		</div>
		<div id="listadoContainer" style="padding-top: 50px;">
		<!--
			<p class='tituloDesplegable'><a href='javascript:tituloPlegable(2)'><span id='iconoPlegable2'><i class='far fa-caret-square-down'></i></span> Consultas Sin Asignar</a></p>
			<span id='cursoPlegable2'>
			<h3>Actualmente no hay ninguna consulta pendiente de asignar.</h3>
				<div class='tarjetaContainer'>
					<div class='datosContainer' style='width: auto; margin-left: 25px;'>
						<h2>Titulo</h2>
						<p>Descripcion</p>
						<h4>Usuario</h4>
					</div>
					<div class='botonesContainer'>
						<button style='color: #000; background-color: #9FD120;'>Asignar</button>
						<button style='background-color: #C0392B;'>Eliminar</button>
					</div>
				</div>
			</span>
		-->

		<div id="misConsultas">
			<?php
				echo $consulta->listaMisConsultas($_SESSION['user']);
			?>
		</div>

		<div id="otrasConsultas">
			<?php
				echo $consulta->listaOtrasConsultas($_SESSION['user']);
			?>
		</div>
		
		</div>
		<form name="eliminaConsulta" action="../src/controlador/consulta.main.php">
			<input type="hidden" name="opcion" value="4">
			<input type="hidden" name="id_consulta">
		</form>
	</section>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaConsulta.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>