<?php 
require 'includes/verificacion.inc.php';

$usuario = new Usuario();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $usuario->obtenerUsuarioPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Usuario | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="usuario_form" action="../src/controlador/usuario.main.php?opcion=9" method="post">
				<input type="hidden" name="id" value="<?php echo ($usuario->getId()!= NULL)?$usuario->getId():"0" ?>">
				<div id="formDatos">
					<ul id="formDatosFoto">
						<li><label>Fotografia</label></li>
						<?php
							if ($usuario->getId()==NULL) {
								echo "<li><div id='formFoto' style='background-image: url(../images/usuarios/plantilla.png)'></div></li>";
							}else{
								echo "<li><div id='formFoto' style='background-image: url(../".$usuario->getFoto().")'></div></li>";
							}
						?>
						<li>
							<input type="hidden" name="foto" id="selectedFoto" value="<?php echo ($usuario->getFoto()!= NULL)?$usuario->getFoto():"images/usuarios/plantilla.png" ?>">
							<input type="file" name="selectedFile" id="selectedFile" style="display: none;" onchange="subirFotoForm('cambiarFotoUser.php')" accept="image/jpeg">
							<input type="button" name="fotoBoton" value="Seleccionar Foto" onclick="document.getElementById('selectedFile').click();" style="border-radius: 5px;">
						</li>
					</ul>
					<ul id="formDatosTxtArea">
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Descripción</label></li>
						<li><textarea name="biografia" onblur="check_form_vacio(this, 0)" placeholder="Escribe una breve descripción" maxlength="160"><?php echo ($usuario->getBiografia()!= NULL)?$usuario->getBiografia():"" ?></textarea></li>
					</ul>
				</div>
				<div id="dobleForm">
					<ul>
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre</label></li>
						<li><input type="text" name="nombre" onblur="check_form_vacio(this, 1)" placeholder="Introduce el nombre" value="<?php echo ($usuario->getNombre()!= NULL)?$usuario->getNombre():"" ?>"></li>
					</ul>
					<ul>
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Apellidos</label></li>
						<li><input type="text" name="apellidos" onblur="check_form_vacio(this, 2)" placeholder="Introduce los apellidos" value="<?php echo ($usuario->getApellidos()!= NULL)?$usuario->getApellidos():"" ?>"></li>
					</ul>
				</div>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Fecha de Nacimiento</label></li>
					<li><input type="date" name="fecha" onblur="check_form_vacio(this, 3)" placeholder="Introduce la fecha de nacimiento" value="<?php echo ($usuario->getFecha_nacimiento()!= NULL)?$usuario->getFecha_nacimiento():"" ?>"></li>
				</ul>

				<?php 
					if ($usuario->getId()==NULL) {
						echo "<div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Correo electrónico</label></li>
							  			<li><input type='text' name='email' onblur='check_form_vacio(this, 4)'' placeholder='Introduce el correo electrónico'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Contraseña</label></li>
							  			<li><input type='password' name='password' onblur='check_form_vacio(this, 5)' placeholder='Introduce la contraseña'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> DNI</label></li>
							  		<li><input type='text' name='dni' onblur='check_form_vacio(this, 6)' placeholder='Introduce el DNI completo'></li>
							  </ul>
							  <div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Dirección</label></li>
							  			<li><input type='text' name='direccion' onblur='check_form_vacio(this, 7)' placeholder='Introduce la dirección'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Codigo Postal</label></li>
							  			<li><input type='text' name='cod_postal' onblur='check_form_vacio(this, 8)' placeholder='Introduce el código postal'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Pais</label></li>
							  		<li><input type='text' name='pais' onblur='check_form_vacio(this, 9)' placeholder='Introduce el pais'></li>
							  </ul>
							  <div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Poblacion</label></li>
							  			<li><input type='text' name='poblacion' onblur='check_form_vacio(this, 10)' placeholder='Introduce la población'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Provincia</label></li>
							  			<li><input type='text' name='provincia' onblur='check_form_vacio(this, 11)' placeholder='Introduce la provincia'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Telefono movil</label></li>
							  		<li><input type='text' name='movil' onblur='check_form_vacio(this, 12)' placeholder='Introduce el movil'></li>
							  </ul>
							  <div id='dobleForm'>
							  		<ul>
							  			<li><label><span><i class='fas fa-exclamation-circle'></i></span> Twitter</label></li>
							  			<li><input type='text' name='poblacion' placeholder='Introduce el twitter (opcional)'></li>
							  		</ul>
							  		<ul>
							  			<li><label><span><i class='fas fa-exclamation-circle'></i></span> LinkedIn</label></li>
							  			<li><input type='text' name='provincia' placeholder='Introduce el linkedin (opcional)'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label>Otro telefono</label></li>
							  		<li><input type='text' name='telefono' placeholder='Introduce otro telefono (opcional)'></li>
							  </ul>
							  <ul>
						      	<li>Selecciona un Tema</li>
						      	<li>
						      		<div id='usuarioTema'>
						      			<input type='button' name='temaBtn1' class='check' style='background-color: #152770; border-color: #F39C12;' onclick='adminTema(0)'>
						      			<input type='button' name='temaBtn2' class='check' style='background-color: #C0392B;' onclick='adminTema(1)'>
						      			<input type='button' name='temaBtn3' class='check' style='background-color: #58B616;' onclick='adminTema(2)'>
						      			<input type='button' name='temaBtn4' class='check' style='background-color: #8E44AD;' onclick='adminTema(3)'>
						      		</div>
						      	</li>
						      	<input type='radio' name='tema' value='1' style='display: none;' checked>
						      	<input type='radio' name='tema' value='2' style='display: none;'>
						      	<input type='radio' name='tema' value='3' style='display: none;'>
						      	<input type='radio' name='tema' value='4' style='display: none;'>
						      </ul>
						      <input id='adminGuardar' type='button' name='enviar' onclick='document.usuario_form.submit()' value='Crear'>";
					}else{
						echo "<div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Dirección</label></li>
							  			<li><input type='text' name='direccion' onblur='check_form_vacio(this, 4)' placeholder='Introduce la dirección' value='".strval(($usuario->getDireccion()!= NULL)?$usuario->getDireccion():"")."'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Codigo Postal</label></li>
							  			<li><input type='text' name='cod_postal' onblur='check_form_vacio(this, 5)' placeholder='Introduce el código postal' value='".strval(($usuario->getCod_postal()!= NULL)?$usuario->getCod_postal():"")."'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Pais</label></li>
							  		<li><input type='text' name='pais' onblur='check_form_vacio(this, 6)' placeholder='Introduce el pais' value='".strval(($usuario->getPais()!= NULL)?$usuario->getPais():"")."'></li>
							  </ul>
							  <div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Poblacion</label></li>
							  			<li><input type='text' name='poblacion' onblur='check_form_vacio(this, 7)' placeholder='Introduce la población' value='".strval(($usuario->getPoblacion()!= NULL)?$usuario->getPoblacion():"")."'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Provincia</label></li>
							  			<li><input type='text' name='provincia' onblur='check_form_vacio(this, 8)' placeholder='Introduce la provincia' value='".strval(($usuario->getProvincia()!= NULL)?$usuario->getProvincia():"")."'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Monedas</label></li>
							  		<li><input type='text' name='monedas' onblur='check_form_vacio(this, 9)' placeholder='Introduce el numero de monedas' value='".strval(($usuario->getMonedas()!= NULL)?$usuario->getMonedas():"")."'></li>
							  </ul>
							  <div id='dobleForm'>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Twitter</label></li>
							  			<li><input type='text' name='twitter' onblur='check_form_vacio(this, 10)' placeholder='Introduce el twitter (opcional)' value='".strval(($usuario->getTwitter()!= NULL)?$usuario->getTwitter():"")."'></li>
							  		</ul>
							  		<ul>
							  			<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> LinkedIn</label></li>
							  			<li><input type='text' name='linkedin' onblur='check_form_vacio(this, 11)' placeholder='Introduce el linkedin (opcional)' value='".strval(($usuario->getLinkedin()!= NULL)?$usuario->getLinkedin():"")."'></li>
							  		</ul>
							  </div>
							  <ul>
							  		<li><label class='labelForm'><span class='spanForm'><i class='fas fa-exclamation-circle'></i></span> Telefono movil</label></li>
							  		<li><input type='text' name='movil' onblur='check_form_vacio(this, 12)' placeholder='Introduce el movil' value='".strval(($usuario->getMovil()!= NULL)?$usuario->getMovil():"")."'></li>
							  </ul>
							  <ul>
							  		<li><label>Otro telefono</label></li>
							  		<li><input type='text' name='telefono' placeholder='Introduce otro telefono (opcional)' value='".strval(($usuario->getTelefono()!= NULL)?$usuario->getTelefono():"")."'></li>
							  </ul>
							  <div id='tripleForm' style='text-align: center;'>
							  	<ul>
							  		<li><label>Administrador</label></li>
							  		<li><input type='checkbox' name='admin'".strval(($usuario->getAdmin()==1)?" checked>":">")."</li>
							  	</ul>
							  	<ul>
							  		<li><label>Profesor</label></li>
							  		<li><input type='checkbox' name='profesor'".strval(($usuario->getProfesor()==1)?" checked>":">")."</li>
							  	</ul>
							  	<ul>
							  		<li><label>Verificado</label></li>
							  		<li><input type='checkbox' name='verificado'".strval(($usuario->getVerificado()==1)?" checked>":">")."</li>
							  	</ul>
							  </div>
							  <input id='adminGuardar' type='button' name='enviar' onclick='valida_usuario_form()' value='Actualizar'>";
					}
				?>
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="usuariosList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>