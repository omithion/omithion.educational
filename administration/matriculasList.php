<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/matricula.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Matriculas | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarMatricula')">
			</form>
		</div>
		<div id="listadoContainer">
			<?php
				$matricula = new Matricula();
				echo ($matricula->listarMatriculas());
			?>
		<!--
			<div class='tarjetaContainer'>
				<div class='datosContainer' style='width: auto; margin-left: 25px;'>
					<h3>Nombre Usuario</h3>
					<p style='color: #BA4A00;'><b>Curso</b></p>
					<p style='margin-bottom: 2.5px;'><i class='fas fa-cash-register'></i> Pago con Tarjeta</p>
					<p style='margin-top: 2.5px;'><i class='fas fa-calendar-alt'></i> Creada/Modificada el 24/05/2019</p>
					<p><b>Notificaciones activas</b> - <span style="color: #C0392B;"><i class="fas fa-times-circle"></i></span></p>
				</div>
				<div class='botonesContainer'>
					<button style='color: #000;'>Editar</button>
					<button style='background-color: #C0392B;'>Borrar</button>
				</div>
			</div>
		-->
		</div>
		<form name="eliminaMatricula" action="../src/controlador/matricula.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="id_matricula">
		</form>
	</section>
	<div id="addForm">
		<a href="matriculasForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaMatricula.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>