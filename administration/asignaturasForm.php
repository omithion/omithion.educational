<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/curso.class.php';
require '../src/modelo/asignatura.class.php';

$asignatura = new Asignatura();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $asignatura->obtenerAsignaturaPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita una Asginatura | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="asignatura_form" action="../src/controlador/asignatura.main.php?opcion=1" method="post">
				<input type="hidden" name="id" value="<?php echo ($asignatura->getId()!= NULL)?$asignatura->getId():"0" ?>">
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre de la Asignatura</label></li>
					<li><input type="text" name="nombre" onblur="check_form_vacio(this, 0)" placeholder="Introduce la asignatura" value="<?php echo ($asignatura->getNombre()!= NULL)?$asignatura->getNombre():"" ?>"></li>
				</ul>
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Curso al que pretenece</label></li>
					<li>
						<select name="curso">
							<?php
								$curso = new Curso();
								echo $curso->selectorCursos(($asignatura->getId_curso()!= NULL)?$asignatura->getId_curso():0);
							?>
						</select>
					</li>
				</ul>
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Profesor Asignado</label></li>
					<li>
						<select name="profesor">
							<?php
								$usuario = new Usuario();
								echo $usuario->selectorProfesores(($asignatura->getId_usuario_profesor()!= NULL)?$asignatura->getId_usuario_profesor():0);
							?>
						</select>
					</li>
				</ul>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.asignatura_form.submit()" value="<?php echo ($asignatura->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="asignaturasList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>