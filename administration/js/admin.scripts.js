var bBusqueda = getXMLHTTPRequest();
function barraBusqueda(ruta) {
    var nombreBuscado = document.getElementById('nombreBuscado').value;
    var myurl = '../src/llamadas/'+ruta+'.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&nombre='+nombreBuscado;
    bBusqueda.open("GET", modurl, true); //true es que la conexion es asincrona
    bBusqueda.onreadystatechange = barraBusquedaResponse;
    bBusqueda.send(null);
}
function barraBusquedaResponse() {
    if (bBusqueda.readyState == 4) {
        if (bBusqueda.status == 200) {
            var miTexto = bBusqueda.responseText;

            if (miTexto == "") {
                document.getElementById('listadoContainer').innerHTML = "<h1>No existen coincidencias con ese nombre</h1>";
            }else{
                document.getElementById('listadoContainer').innerHTML = miTexto;
            }
        }
    }
}

var bCambiaFoto = getXMLHTTPRequest();
function subirFotoForm(ruta) {
    var fotoAntigua = document.getElementById('selectedFoto').value;
    var myurl = "../src/llamadas/"+ruta;
    
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&fotoAntigua='+fotoAntigua;

    bCambiaFoto.open("POST", modurl, true); //true es que la conexion es asincrona
    bCambiaFoto.onreadystatechange = subirFotoFormResponse;;

    var fileInput = document.getElementById('selectedFile');
    var file = fileInput.files[0];
    var formData = new FormData();
    formData.append('foto', file);

    bCambiaFoto.send(formData);
}
function subirFotoFormResponse() {
    if (bCambiaFoto.readyState == 4) {
        if (bCambiaFoto.status == 200) {
            var miTexto = bCambiaFoto.responseText;

            if (miTexto != "grande") {
                if (miTexto == "tipo") {
                    alertaPantalla("Archivo no reconocido.", "El archivo que intentas subir no se corresponde al de una imagen.", "Intentalo de nuevo con un archivo de tipo .jpg, .jpeg o .png.");
                }else{
                    document.getElementById('formFoto').style.backgroundImage = "url(../"+miTexto+")";
                    document.getElementById('selectedFoto').value = miTexto;
                }
            }else {
                alertaPantalla("Archivo demasiado grande", "El archivo no puede superar los 2 Mb de tamaño.", "Intentalo de nuevo con un archivo mas pequeño.");
            }
        }
    }
}

var bSubeArchivo = getXMLHTTPRequest();
function subirArchivoForm(num, ruta) {
    document.getElementById('numSelect').value = num;
    var archivoAntiguo = document.getElementById('archivo'+num).value;
    var myurl = "../src/llamadas/"+ruta+".php";
    
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&archivoAntiguo='+archivoAntiguo;

    bSubeArchivo.open("POST", modurl, true); //true es que la conexion es asincrona
    bSubeArchivo.onreadystatechange = subirArchivoFormResponse;;

    var fileInput = document.getElementById('fileSelected'+num);
    var file = fileInput.files[0];
    var formData = new FormData();
    formData.append('archivo', file);

    bSubeArchivo.send(formData);
}
function subirArchivoFormResponse() {
    if (bSubeArchivo.readyState == 4) {
        if (bSubeArchivo.status == 200) {
            var miTexto = bSubeArchivo.responseText;
            var num = document.getElementById('numSelect').value;
            
            if (miTexto != "grande") {
                document.getElementById('spanImage'+num).innerHTML = "<a href='../"+miTexto+"' download><span style='background-image: url(../images/archivos/"+miTexto.split('.').pop()+".png);''></span></a>";
                document.getElementById('botonSelected'+num).style.backgroundColor = "#1E8449";
                if (miTexto.substring(15,-1) == "files/temarios/") document.getElementById('fileName'+num).innerHTML = miTexto.substr(25);
                else document.getElementById('fileName'+num).innerHTML = miTexto.substr(28);
                document.getElementById('fileName'+num).style.wordBreak = "break-all";
                document.getElementById('archivo'+num).value = miTexto;
            }else {
                alertaPantalla("Archivo demasiado grande", "El archivo no puede superar los 2 Mb de tamaño.", "Intentalo de nuevo con un archivo mas pequeño.");
            }
        }
    }
}

function borraCurso(id) {
    confirmacionPantalla("este", "curso");
    document.eliminaCurso.id_curso.value = id;
}
function borraUsuario(id) {
    confirmacionPantalla("este", "usuario");
    document.eliminaUsuario.id_usuario.value = id;
}
function borraAsignatura(id) {
    confirmacionPantalla("esta", "asignatura");
    document.eliminaAsignatura.id_asignatura.value = id;
}
function borraConsulta(id) {
    confirmacionPantalla("esta", "consulta");
    document.eliminaConsulta.id_consulta.value = id;
}
function borraTemario(id) {
    confirmacionPantalla("este", "temario");
    document.eliminaTemario.id_temario.value = id;
}
function borraActividad(id) {
    confirmacionPantalla("esta", "actividad");
    document.eliminaActividad.id_actividad.value = id;
}
function borraMatricula(id) {
    confirmacionPantalla("esta", "matricula");
    document.eliminaMatricula.id_matricula.value = id;
}
function borraTema(id) {
    confirmacionPantalla("este", "tema");
    document.eliminaTema.id_tema.value = id;
}

function confirmacionPantalla(esta, categoria) {
    document.getElementById('confirmacionPantalla').style.display = "inline-block";
    document.getElementById('confirmacionMensaje').style.display = "inline-block";
    document.getElementById('confirmacionH2').innerHTML = "Eliminar "+categoria;
    document.getElementById('confirmacionH3').innerHTML = "¿Estas seguro de que deseas eliminar "+esta+" "+categoria+" de la aplicacion?";
    document.getElementById('confirmacionP').innerHTML = "Una vez se haya eliminado no se podra recuperar nuevamente";
}

function cerrarConfirmacion() {
    document.getElementById('confirmacionPantalla').style.display = "none";
    document.getElementById('confirmacionMensaje').style.display = "none";
}


function adminTema(num) {
    document.usuario_form.tema[num].checked = true;
    document.getElementsByClassName('check')[0].style.borderColor = "#909497";
    document.getElementsByClassName('check')[1].style.borderColor = "#909497";
    document.getElementsByClassName('check')[2].style.borderColor = "#909497";
    switch (num) {
        case 0:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#152770";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#152770";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#152770B3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 1:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#C0392B";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#C0392B";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#C0392BB3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 2:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#58B616";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#58B616";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#58B616B3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 3:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#8E44AD";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#8E44AD";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#8E44ADB3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
    }
}

function check_form_vacio(elemento, pos){
    if (elemento.value == "") {
        elemento.style.borderColor = "red";
        document.getElementsByClassName('labelForm')[pos].style.color = "red";
        document.getElementsByClassName('spanForm')[pos].style.display = "inline-block";
    }else{
        elemento.style.borderColor = "black";
        document.getElementsByClassName('labelForm')[pos].style.color = "black";
        document.getElementsByClassName('spanForm')[pos].style.display = "none";
    }
}

function valida_usuario_form() {
    if (document.usuario_form.admin.checked && document.usuario_form.profesor.checked) {
        alertaPantalla("Error en los roles de usuario", "Un usuario no puede ser Administrador y Profesor al mismo tiempo.", "Intentalo de nuevo quitando uno de los dos roles al usuario.");
    }else{
        document.usuario_form.submit();
    }
}

function tituloPlegable(curso) {
    if (document.getElementById('cursoPlegable'+curso+'').style.display == "none") {
        document.getElementById('cursoPlegable'+curso+'').style.display="block"
        document.getElementById('iconoPlegable'+curso+'').innerHTML = "<i class='far fa-caret-square-down'></i>";
    }else{
        document.getElementById('cursoPlegable'+curso+'').style.display="none";
        document.getElementById('iconoPlegable'+curso+'').innerHTML = "<i class='far fa-caret-square-right'></i>";
    }
}

var bRecuperacionAdmin = getXMLHTTPRequest();
function recuperacionAdmin(email) {
    misdatos = "email="+email;

    var myurl = '../src/controlador/usuario.main.php?opcion=12';
    bRecuperacionAdmin.open("POST", myurl, true); //true es que la conexion es asincrona
    bRecuperacionAdmin.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bRecuperacionAdmin.onreadystatechange = recuperacionAdminResponse;
    bRecuperacionAdmin.send(misdatos);
}
function recuperacionAdminResponse() {
    if (bRecuperacionAdmin.readyState == 4) {
        if (bRecuperacionAdmin.status == 200) {
            var miTexto = bRecuperacionAdmin.responseText;
            
        }
    }
}