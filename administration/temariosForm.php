<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/asignatura.class.php';
require '../src/modelo/temario.class.php';

$temario = new Temario();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $temario->obtenerTemarioPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Temario | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="temario_form" action="../src/controlador/temario.main.php?opcion=1" method="post">
				<input type="hidden" name="url" value="administration/temariosList.php">
				<input type="hidden" name="id" value="<?php echo ($temario->getId()!= NULL)?$temario->getId():"0" ?>">
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Asignatura al que pretenece</label></li>
					<li>
						<select name="asignatura">
							<?php
								$asignatura = new Asignatura();
								echo $asignatura->selectorAsignaturas(($temario->getId_asignatura()!= NULL)?$temario->getId_asignatura():0);
							?>
						</select>
					</li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre del Temario</label></li>
					<li><input type="text" name="nombre" onblur="check_form_vacio(this, 0)" placeholder="Introduce un nombre" value="<?php echo ($temario->getNombre()!= NULL)?$temario->getNombre():"" ?>"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Contenido del Temario</label></li>
					<li><textarea name="contenido" onblur="check_form_vacio(this, 1)" placeholder="Introduce el contenido que tendrá" maxlength="160"><?php echo ($temario->getContenido()!= NULL)?$temario->getContenido():"" ?></textarea></li>
				</ul>
				<div id="fileForm">
					<input id='numSelect' type='hidden'>
					<?php
						if ($temario->getId()!= NULL) {
							echo $temario->obtenerArchivos($temario->getId());
						}else{
							echo $temario->pintarArchivo(1);
							echo $temario->pintarArchivo(2);
							echo $temario->pintarArchivo(3);
						}
					?>
				</div>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.temario_form.submit()" value="<?php echo ($temario->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="temariosList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>