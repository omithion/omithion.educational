<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/foro.class.php';

$foro = new Foro();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $foro->obtenerForoPorId($id);
}else{
    header("location:forosList.php?asignatura=".$_GET['asignatura']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Foro | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
    <?php include 'includes/header.inc.php';?>
	<section>
		<div class='tarjetaCampus'>
            <h3><i class='fas fa-bookmark'></i> <?php echo $foro->getTitulo(); ?></h3>
            <h5><?php echo "Creado el  ".$foro->converirFecha($foro->getFecha()); ?></h5>
            <p><?php echo $foro->getDescripcion(); ?></p>
            <ul>
            	<?php
            		echo $foro->archivosCampus("SELECT archivo FROM Archivo_Foro WHERE id_foro = ".$foro->getId().";");
            	?>
            </ul>
            <button onclick="muestraComentario()">Comentar</button>
        </div>

        <div id="newComentarioCampus">
            <form name="comentario_form" action="../src/controlador/foro.main.php?opcion=3" method="post">
                <h2 id="tituloH2">Nuevo Comentario</h2>
                <ul>
                    <li><label id="comentarioLabel">Tu Respuesta</label></li>
                    <li>
                        <input type="hidden" name="curso" value="<?php echo $_GET['curso']; ?>">
                        <input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura']; ?>">
                        <input type="hidden" name="foro" value="<?php echo $_GET['foro']; ?>">
                        <textarea name="comentario" placeholder="Escribe aqui tu respuesta" maxlength="140"></textarea>
                    </li>
                </ul>
                <div id="fileForm">
                    <input id="numSelect" type="hidden">
                    <ul>
                        <li id="spanImage1" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName1" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected1" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected1').click()" value="Subir Archivo"></li>
                        <li><input id="archivo1" name="archivo1" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected1" type="file" style="display: none;" onchange="subirArchivoForm(1, 'cambiarArchivoForo')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage2" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName2" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected2" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected2').click()" value="Subir Archivo"></li>
                        <li><input id="archivo2" name="archivo2" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected2" type="file" style="display: none;" onchange="subirArchivoForm(2, 'cambiarArchivoForo')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage3" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName3" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected3" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected3').click()" value="Subir Archivo"></li>
                        <li><input id="archivo3" name="archivo3" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected3" type="file" style="display: none;" onchange="subirArchivoForm(3, 'cambiarArchivoForo')"></li>
                    </ul>
                </div>
                <input type="button" value="Enviar" onclick="nuevoComentario()">
            </form>
        </div>

        <h2 id="tituloH2"><i class="fas fa-comments"></i> Comentarios del foro</h2>

        <?php
            echo $foro->foroComentariosListProfesor($foro->getId());
        ?>

	</section>
    <div id="backList">
        <a href="<?php echo 'forosList.php?asignatura='.$_GET['asignatura'] ?>"><i class="fas fa-arrow-alt-circle-left"></i></a>
    </div>
    <div id="alertaPantalla" onclick="cerrarAlerta()"></div>
    <div id="alertaMensaje">
        <h2 id="alertaH2"></h2>
        <h3 id="alertaH3"></h3>
        <p id="alertaP"></p>
        <button onclick="cerrarAlerta()">Cerrar</button>
    </div>
    <?php include 'includes/footer.inc.php';?>
</body>
</html>