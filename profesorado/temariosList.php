<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/temario.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Temarios | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		.botonesContainer button{
			width: 100%;
			margin: 10px 0;
		}
		.datosContainer {
    			width: 80%;
    			margin-left: 25px;
			}
		@media screen and (max-width: 1920px){
			.datosContainer {
    			width: 80%;
			}
		}
		@media screen and (max-width:1024px){
			.datosContainer{
				width: 100%;
				margin: 0;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda" name="formBusqueda">
				<input type="text" name="busqueda">
				<input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura'] ?>">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusquedaProfesor('buscarTemarioProfesor')">
			</form>
		</div>
		<div id="listadoContainer" style="padding-top: 50px;">
			<?php
				$temario = new Temario();
				echo $temario->listarTemariosProfesor($_GET['asignatura']);
			?>		
		</div>
		<form name="eliminaTemario" action="../src/controlador/temario.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="url" value="<?php echo 'profesorado/temariosList.php?asignatura='.$_GET['asignatura'] ?>">
			<input type="hidden" name="id_temario">
		</form>
	</section>
	<div id="addForm">
		<a href="<?php echo 'temariosForm.php?asignatura='.$_GET['asignatura'] ?>"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaTemario.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>