<?php
	require_once '../src/modelo/usuario.class.php';
    require_once '../src/modelo/matricula.class.php';
    require_once '../src/modelo/asignatura.class.php';
    require_once '../src/conector/bd.class.php';

    session_start();
    if (!isset($_SESSION['user'])) {
        header("location:../login.php");
    }elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
        header("location:../administration/index.php");
    }elseif (!Usuario::usuarioProfesor($_SESSION['user'])) {
        header("location:../index.php");
    }

	$usuario = new Usuario();
	$id = $usuario->obtenerIdPorEmail($_SESSION['user']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ayuda | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion4').className = "activo";
		}
		function mostrarconsulta() {
			document.getElementById('ayudaLista').style.display = "none";
			document.getElementById('formConsulta').style.display = "inline-block";
		}
		function check_form_vacio(elemento, pos){
		    if (elemento.value == "") {
		        elemento.style.borderColor = "red";
		        document.getElementsByClassName('labelForm')[pos].style.color = "red";
		        document.getElementsByClassName('spanForm')[pos].style.display = "inline-block";
		    }else{
		        elemento.style.borderColor = "black";
		        document.getElementsByClassName('labelForm')[pos].style.color = "black";
		        document.getElementsByClassName('spanForm')[pos].style.display = "none";
		    }
		}
		function compruebaConsulta(argument) {
			var ok = true;
    		var formulario = document.formConsulta;
    		if(!formulario.titulo.value){
    			check_form_vacio(formulario.titulo, 0);
    		    ok = false;
    		}
    		if(!formulario.descripcion.value){
    			check_form_vacio(formulario.descripcion, 1);
    		    ok = false;
    		}
    		if (ok) {
    		    formulario.submit();
    		}
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section id="ayudaView">
		<div id="ayudaLista">
			<h1>Bienvenido a la pagina de ayuda</h1>
			<p style="text-align: center;">Aqui encontrarás una pequeña guia de uso de la aplicación, si necesitas hacer alguna duda al soporte no dudes en rellenar el formulario de consulta.</p>
			<button onclick="mostrarconsulta()">Enviar Consulta</button>
		
			<h3><a href='javascript:ayudaPlegable(1)'><span id='iconoPlegable1'><i class='fas fa-chevron-circle-right'></i></span> Crear un nuevo temario</a></h3>
    	    <span id='ayudaPlegable1' style="display: none;">
    	    	<p>Para crear un nuevo temario debes acceder a una de tus asignaturas y posteriormente deberas acceder a la parte de temarios.</p>
    	    	<img src="../images/ayuda/ejemplo.jpg">
    	    	<p>Una vez hayas accedido a la parte de temarios deberás darle al bótón con el símbolo "+", completar el formulario requerido y darle al botón de guardar.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(2)'><span id='iconoPlegable2'><i class='fas fa-chevron-circle-right'></i></span> Crear una nueva actividad</a></h3>
    	    <span id='ayudaPlegable2' style="display: none;">
    	    	<p>Para crear una nueva actividad deberás acceder a una de las asignaturas y después deberás pulsar en el botón de actividades.</p>
    	    	<img src="../images/ayuda/ejemplo.jpg">
    	    	<p>Una vez hayas accedido a la parte de actividades deberás darle al bótón con el símbolo "+", completar el formulario requerido y darle al botón de guardar.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(3)'><span id='iconoPlegable3'><i class='fas fa-chevron-circle-right'></i></span> Crear un foro</a></h3>
    	    <span id='ayudaPlegable3' style="display: none;">
    	    	<p>Para crear un foro deberás acceder a la asignatura en la que quieras crearlo. Una vez allí acceder al apartado de foros.</p>
    	    	<img src="../images/ayuda/ejemplo.jpg">
    	    	<p>Una vez hayas accedido a la parte de foros deberás darle al bótón con el símbolo "+", completar el formulario requerido y darle al botón de guardar. Ya tienes disponible tu nuevo foro.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(3)'><span id='iconoPlegable3'><i class='fas fa-chevron-circle-right'></i></span> Crear una tutoría</a></h3>
    	    <span id='ayudaPlegable3' style="display: none;">
    	    	<p>Para crear una tutoría deberás acceder a la asignatura en la que quieres crearla. Una vez allí pulsarás en la sección de tutorías.</p>
    	    	<img src="../images/ayuda/ejemplo.jpg">
    	    	<p>Cuando hayas accedido a la parte de tutorías deberás darle al bótón con el símbolo "+", completar el formulario requerido y seleccionar al alumno en cuestión. Después pulsarás el botón de guardar. Ya tienes disponible tu nuevo foro.</p>
    	    </span>
		</div>

		<div id="formConsulta">
			<h1>Consulta al Soporte</h1>
			<p style="text-align: center;">Completa el siguiente formulario para cualquier consulta o problema que tengas con la aplicación. El soporte te contestará via e-mail en cuanto sea posibe.</p>
			<form name="formConsulta" action="../src/controlador/consulta.main.php?opcion=1" method="post">
				<input type="hidden" name="id" value="<?php echo $id; ?>">
				<input type="hidden" name="url" value="profesorado/ayuda.php">
				<ul>
					<li><label class="labelForm"><span class="spanForm" style="display: none;"><i class="fas fa-exclamation-circle"></i></span> Título</label></li>
					<li><input type="text" name="titulo" placeholder="Titulo de la consulta" onblur="check_form_vacio(this, 0)"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm" style="display: none;"><i class="fas fa-exclamation-circle"></i></span> Descripción de la consulta</label></li>
					<li><textarea name="descripcion" placeholder="Describe aqui tu consulta" onblur="check_form_vacio(this, 1)" maxlength="250"></textarea></li>
				</ul>
				<input type="button" onclick="compruebaConsulta()" name="enviar" value="Enviar">
			</form>
		</div>
	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>